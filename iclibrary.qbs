import qbs 1.0

DynamicLibrary {
    property string incpath
    property string srcpath
    property string installPath: ""
    Depends { name:"cpp" }
    Depends { name: "Qt.core" }
    cpp.cxxFlags: ['-std=c++11']
    cpp.linkerFlags: ['-Wl,--rpath=./,--no-undefined']			
    cpp.includePaths: qbs.targetOS.contains("windows")?[incpath, "C:/Boost"]:[incpath]

    destinationDirectory: "bin"
    Group {
        qbs.install: true
        qbs.installDir: installPath
        fileTagsFilter: "dynamiclibrary"
    }
}
