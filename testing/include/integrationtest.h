#ifndef INTEGRATIONTEST_H
#define INTEGRATIONTEST_H

#include <QObject>
#include <ictest_global.h>

#define DEFAULT_VERIFY_TIMEOUT 3000

/**
 * @brief Класс для интеграционных тестов
 */
class ICTESTSHARED_EXPORT IntegrationTest : public QObject
{
    Q_OBJECT

public:
    explicit IntegrationTest(QObject *parent = 0);
public slots:
    /**
     * @brief установить флаг успешного завершения
     */
    inline void succeed(){m_success = true;}
protected:
    /**
     * @brief Начать тест
     * @param testCase номер теста
     */
    void begin(int testCase);
    /**
     * @brief Ожидать изменения m_success
     * @param timeout время ожидания результата
     */
    void verify(int timeout = DEFAULT_VERIFY_TIMEOUT);
    /**
     * @brief Номер текущего теста
     * @return
     */
    inline int currentTest() const{return m_currentTest;}
private:
    bool m_success;
    int m_currentTest;
};

#endif // INTEGRATIONTEST_H
