#ifndef ICMETAOBJECTRECORD_H
#define ICMETAOBJECTRECORD_H

#include <icrecord.h>
#include <QObject>

/**
 * @brief Реализация интерфейса контейнера через QMetaObject.
 *
 * Может служить контейнером для своего родителя, но если в конструкторе не указывается parent,
 * служит контейнером для самого себя.
 * @ingroup core database
 */
class ICCONTAINERSSHARED_EXPORT ICMetaObjectRecord : public ICRecord
{
public:
    ICMetaObjectRecord(QObject* target = 0);
    ~ICMetaObjectRecord();
    QStringList names() const Q_DECL_FINAL;
    QVariant value(const QString& name) const Q_DECL_FINAL;
    void setValue(const QString& name,const QVariant& value) Q_DECL_FINAL;
    void serializeContent(QDataStream &stream) const Q_DECL_FINAL;
    void deserializeContent(QDataStream &stream) Q_DECL_FINAL;
    QString className() const Q_DECL_FINAL {return "ICMetaObjectRecord";}
    ICRecord* newInstance() const Q_DECL_FINAL {return new ICMetaObjectRecord();}
private:
    QObject* m_target;
    bool m_toFreeTarget;
};

#endif // ICMETAOBJECTRECORD_H
