#ifndef ICKEYVALUEPAIR_H
#define ICKEYVALUEPAIR_H

#include <QString>
#include <QVariant>
#include <QDataStream>
#include <iccontainers_global.h>

/**
 * @brief Класс-контейнер для пары ключ-значение
 * @ingroup core database
 */
class ICCONTAINERSSHARED_EXPORT ICKeyValuePair
{
public:
    ICKeyValuePair(){}
    ICKeyValuePair(const QString& key,const QVariant& value);
    inline const QString& key() const{return m_key;}
    inline const QVariant& value() const{return m_value;}
    inline void setKey(const QString& key){m_key=key;}
    inline void setValue(const QVariant& val){m_value = val;}
private:
    QString m_key;
    QVariant m_value;
};

ICCONTAINERSSHARED_EXPORT QDataStream& operator <<(QDataStream& ds,const ICKeyValuePair& keyval);
ICCONTAINERSSHARED_EXPORT QDataStream& operator >>(QDataStream& ds,ICKeyValuePair& keyval);

#endif // ICKEYVALUEPAIR_H
