#ifndef ICRECORD_H
#define ICRECORD_H

#include "iccontainers_global.h"
#include <QString>
#include <QVariant>
#include "icrecordtypescollector.h"

/**
 * @brief Интерфейс класса-контейнера атрибутов объекта
 * @ingroup core database
 */
class ICCONTAINERSSHARED_EXPORT ICRecord
{
public:
    ICRecord(){}
    virtual ~ICRecord(){}
    /**
     * @brief список имен атрибутов
     * @return
     */
    virtual QStringList names() const = 0;
    /**
     * @brief содержит ли запись атрибут с данным именем
     * @param nm имя атрибута
     * @return
     */
    virtual bool contains(const QString& nm) const{return names().contains(nm);}
    /**
     * @brief значение атрибута по указанному имени
     * @param name имя атрибута
     * @return значение
     */
    virtual QVariant value(const QString& name) const = 0;
    /**
     * @brief установить значение данного атрибута
     * @param name имя атрибута
     * @param value значение
     */
    virtual void setValue(const QString& name,const QVariant& value) = 0;
    /**
     * @brief Сериализовать объект в поток
     * @param ds поток
     * @param record запись
     */
    static void serialize(QDataStream& ds,const ICRecord* record);
    /**
     * @brief Десериализовать объект из потока
     * @param ds поток
     * @return объект класса-наследника ICRecord
     */
    static ICRecord* deserialize(QDataStream& ds);
    /**
     * @brief сериализовать содержимое в поток
     * @param stream поток
     */
    virtual void serializeContent(QDataStream& stream) const = 0;
    /**
     * @brief десериализовать содержимое из потока
     * @param stream
     */
    virtual void deserializeContent(QDataStream& stream) = 0;
    
    /**
     * @brief создать новый объект своего типа
     */
    virtual ICRecord* newInstance() const = 0;

    /**
     * @brief возвращает строку-название типа
     */
    virtual QString className() const = 0;

    /**
     * @brief makeCopy Создает полную копию объекта.
     * @return Копия объекта.
     */
    ICRecord* makeCopy() const;
    /**
     * @brief Список имен измененных полей
     * @return
     */
    virtual QStringList changes() const{return names();}
    /**
     * @brief Подтвердить изменения
     */
    virtual void commit(){}
    /**
     * @brief Откатить изменения
     */
    virtual void rollback(){}
protected:
    friend class ICRecordSet;
};

/**
 * @brief Макрос, регистрирующий тип для класса-наследника ICRecord.
 * 
 * Должен располагаться в cpp-файле регистрируемого класса после #include<...>.
 * @ingroup core database
 */
#define IC_REGISTER_ICRECORDTYPE(typeName) ivk::ICRecordTypeRegistrator<typeName> typeName##_recreg(#typeName);

#endif // ICRECORD_H
