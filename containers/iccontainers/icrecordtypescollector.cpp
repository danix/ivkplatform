#include "icrecordtypescollector.h"
#include "icrecord.h"

using namespace ivk;

ICRecordTypesCollector* ICRecordTypesCollector::g_collectorInstance=nullptr;
ICRecordTypesCollector g_collectorObject;

int ivk::icTypeIndex(const QString& name)
{
    if(!ICRecordTypesCollector::instance()->m_typeHash.contains(name))
    {
        qDebug() << __FILE__<<__LINE__<< QString(" Класс с именем %1 не зарегистрирован!").arg(name);
        return -1;
    }
    return ICRecordTypesCollector::instance()->m_typeHash.value(name);
}

ICRecord* ivk::icNewInstanceByName(const QString& name)
{
    return ICRecordTypesCollector::instance()->m_typeInstances[ICRecordTypesCollector::instance()->m_typeHash.value(name)]->newInstance();
}

ICRecord* ivk::icNewInstance(int typeId)
{
    return ICRecordTypesCollector::instance()->m_typeInstances[typeId]->newInstance();
}
