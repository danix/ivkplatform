import qbs 1.0
import "iclibrary.qbs" as ICLibrary
import "icmodule.qbs" as ICModule

import "core/core.qbs" as CoreModule

Project
{
    id: platform
    name: "platform"
    property string subfolder: ""
    property string coreInclude: subfolder+core.includes
    property string tmpiInclude: subfolder+tmpi.includes
    property string containersInclude: subfolder+containers.includes
    property string uiInclude: subfolder+ui.includes
    property string dbInclude: subfolder+database.includes
    property string authInclude: subfolder+auth.includes

    CoreModule { id: core
        subfolder: "core/"
    }
    ICModule {
        id: tmpi
        name: "tmpi"
        subfolder: "ictmplugininterface/"
        pubincludes: "include/"
        ICLibrary {
            name: "ictmplugininterface"
            incpath: includes
            srcpath: subfolder+"/src/"
            files: [
                srcpath+"iccommand.cpp",
                srcpath+"ictmplugininterface.cpp",
                incpath+"iccommand.h",
                incpath+"ictmplugininterface.h",
                incpath+"ictmplugininterface_global.h",
            ]
            cpp.includePaths: [includes, core.includes]
            Depends {name: "ivkplatform"}
            Depends {name: "icobject"}
            cpp.defines: ["ICTMPLUGININTERFACE_LIBRARY"]
        }
    }
    ICModule {
        id: containers
        name: "containers"
        subfolder: "containers/"
        pubincludes: "include/"
        ICLibrary{
            name: "iccontainers"
            incpath: includes
            srcpath: subfolder+"iccontainers/"
            files: [
                incpath+"icbinaryrecord.h",
                incpath+"iccontainers_global.h",
                incpath+"ickeyvaluepair.h",
                incpath+"icmetaobjectrecord.h",
                incpath+"icrecord.h",
                incpath+"icrecordset.h",
                incpath+"icrecordtypescollector.h",
                incpath+"icsettingscontainer.h",
                incpath+"icvaluemap.h",
                incpath+"icsharedrecord.h",
                srcpath+"*.cpp"
            ]
            cpp.defines: ["ICCONTAINERS_LIBRARY"]
        }
        ICLibrary{
            name: "icobjectrecord"
            incpath: includes
            srcpath: subfolder+"icobjectrecord/"
            files: [
                incpath+"icobjectrecord_global.h",
                incpath+"icobjectrecord.h",
                srcpath+"*.cpp"
            ]
            cpp.includePaths: [includes, core.includes]
            cpp.defines: ["ICOBJECTRECORD_LIBRARY"]
            Depends {name: "icobject"}
            Depends {name: "iccontainers"}
        }
    }
    ICModule {
        id: auth
        name: "auth"
        subfolder: "auth/"
        pubincludes: "include/"
        ICLibrary{
            name: "icauth"
            incpath: includes
            srcpath: subfolder+"src/icauth/"
            files: [
                srcpath+"icpasswdsecret.cpp",
                srcpath+"icrole.cpp",
                srcpath+"icsession.cpp",
                srcpath+"iccredentials.cpp",
                srcpath+"icsecret.cpp",
                srcpath+"icpermission.cpp",
                incpath+"iccredentials.h",
                incpath+"icauth_global.h",
                incpath+"icsecret.h",
                incpath+"icuserinfo.h",
                incpath+"icuserphoto.h",
                incpath+"icpasswdsecret.h",
                incpath+"icrole.h",
                incpath+"icpermission.h",
                incpath+"icsession.h",
                incpath+"icauthrequest.h",
                incpath+"icauthcommandstatuses.h",
                incpath+"icsecretcontainer.h",
                srcpath+"icsecretcontainer.cpp",
                srcpath+"icuserinfo.cpp",
                srcpath+"icuserphoto.cpp",
            ]
            cpp.includePaths: [includes, core.includes, containers.includes]
            cpp.defines: ["ICAUTH_LIBRARY"]
            Depends { name: "icobject" }
            Depends { name: "iccontainers" }
            Depends { name: "icobjectrecord" }
        }
        ICLibrary {
            name: "icauthdriver"
            incpath: subfolder+"src/icauthdriver/"
            srcpath: incpath
            files: [
                srcpath+"icauthdriver.cpp",
                incpath+"icauthdriver.h",
                incpath+"icauthdriver_global.h",
                incpath+"icseccompareinterface.h",
                incpath+"icpasswdcomparer.h",
                incpath+"icpasswdcomparer.cpp"
            ]
            cpp.includePaths: [incpath, core.includes, tmpi.includes, auth.includes, database.includes, containers.includes]
            cpp.defines: ["ICAUTHDRIVERLIBRARY"]
            Depends { name: "ictmplugininterface" }
            Depends { name: "icauth" }
            Depends { name: "ivkplatform" }
            Depends { name: "icdatadriverinterface" }
            Depends { name: "icobject" }
            Depends { name: "icobjectrecord" }
            Depends { name: "iccontainers" }
            Depends { name: "icdbrequests" }
            Depends { name: "icdbconnection" }
        }
    }
    ICModule {
        id: database
        name: "database"
        subfolder: "database/"
        pubincludes: "include/"
        ICLibrary {
            name: "icdatadriverinterface"
            incpath: includes
            srcpath: subfolder+"icdatadriverinterface/"
            files: [
                srcpath+"icdatadriverinterface.cpp",
                incpath+"icdatadriverinterface.h",
                incpath+"icdatadriverinterface_global.h"
            ]
            cpp.defines: ["DDI_LIBRARY"]
            cpp.includePaths: [includes, core.includes, tmpi.includes, containers.includes]
            Depends { name: "icobject" }
            Depends { name: "ictmplugininterface" }
            Depends { name: "icdbconnection" }
        }
        ICLibrary {
            name: "icdbconnection"
            incpath: includes
            srcpath: subfolder+"icdbconnection/"
            files: [
                srcpath+"icdbconnection.cpp",
                srcpath+"icdbinformation.cpp",
                srcpath+"icdbconnectionpool.cpp",
                srcpath+"icdbconnpoolmanager.cpp",
                incpath+"icdbconnection_global.h",
                incpath+"icdbconnection.h",
                incpath+"icdbinformation.h",
                incpath+"icdbconnectionpool.h",
                incpath+"icdbconnpoolmanager.h"
            ]
            cpp.defines: ["DBCONNECTION_LIBRARY"]
            cpp.includePaths: [includes, core.includes, containers.includes]
            Depends { name: "icobject" }
            Depends { name: "icobjectrecord" }
            Depends { name: "icsettings" }
            Depends { name: "iccontainers" }
            Depends { name: "icdbrequests" }
        }
        ICLibrary {
            name: "icdbrequests"
            incpath: includes
            srcpath: subfolder+"icdbrequests/"
            files: [
                incpath+"icdbrequest.h",
                incpath+"icdbrequests_global.h",
                incpath+"icdbselectrequest.h",
                incpath+"icdbconditionrequest.h",
                incpath+"icdbinsertrequest.h",
                incpath+"icdbupdaterequest.h",
                incpath+"icdbremoverequest.h",
                incpath+"icdbrequestgroup.h",
                incpath+"icdbresponse.h",
                incpath+"iccondition.h",
                incpath+"icconditionset.h",
                incpath+"icdballrequests.h",
                incpath+"icaggregation.h",
                incpath+"icdbexecrequest.h",
                incpath+"icdbupdategrouprequest.h",
                srcpath+"*.cpp"
            ]
            cpp.defines: ["ICDBREQUESTS_LIBRARY"]
            cpp.includePaths: [includes, containers.includes]
            Depends {name: "iccontainers"}
        }
        ICLibrary {
            name: "ichidb"
            incpath: includes
            srcpath: subfolder+"ichidb/"
            files: [
                incpath+"dbselect.h",
                srcpath+"*.cpp",
                incpath+"ichidb_global.h",
                incpath+"icdb.h"
            ]
            cpp.defines: ["ICHIDB_LIBRARY"]
            cpp.includePaths: [includes, core.includes, containers.includes, tmpi.includes]
            Depends {name: "iccontainers"}
            Depends { name: "icdbrequests" }
            Depends { name: "ictmplugininterface" }
            Depends { name: "icobject"}
            Depends { name: "icsettings"}
            Depends { name: "icobjectrecord"}
        }
        ICLibrary {
            name: "icgenericdatadriver"
            incpath: includes
            srcpath: subfolder+"icgenericdatadriver/"
            files: [
                srcpath+"icgenericdatadriver.cpp",
                incpath+"icgenericdatadriver.h",
                incpath+"icgenericdatadriver_global.h"
            ]
            cpp.defines: ["ICGENERICDATADRIVER_LIBRARY"]
            cpp.includePaths: [includes, core.includes, tmpi.includes, containers.includes]
            Depends { name: "icdatadriverinterface" }
            Depends { name: "icdbrequests" }
            Depends { name: "icdbconnection" }
            Depends { name: "iccontainers" }
            Depends { name: "ictmplugininterface" }
            Depends { name: "icobject" }
        }
        ICLibrary {
            name: "icsqlconnection"
            incpath: includes
            srcpath: subfolder+"icsqlconnection/"
            files: [
                srcpath+"icsqlconnection.cpp",
                incpath+"icsqlconnection.h",
                incpath+"icsqlconnection_global.h"
            ]
            cpp.defines: ["ICSQLCONNECTION_LIBRARY"]
            cpp.includePaths: [includes, core.includes, tmpi.includes, containers.includes]
            Depends { name: "Qt.sql" }
            Depends { name: "icdbconnection" }
            Depends { name: "icsettings" }
            Depends { name: "iccontainers" }
            Depends { name: "icobject" }
        }
        ICLibrary {
            name: "icdbserver"
            incpath: includes
            srcpath: subfolder+"icdbserver/"
            installPath: "plugins"
            files: [
                srcpath+"icdbserver.cpp",
                srcpath+"icdbserver.h",
                srcpath+"icdbserver_global.h"
            ]
            cpp.defines: ["ICDBSERVER_LIBRARY"]
            cpp.includePaths: [includes, core.includes, tmpi.includes, containers.includes]
            cpp.linkerFlags: ['-Wl,-rpath=./']		
            Depends { name: "ictmplugininterface" }
            Depends { name: "icdbconnection" }
            Depends { name: "icobject" }
            Depends { name: "icsettings" }
            Depends { name: "icdatadriverinterface" }
            Depends { name: "ivkplatform" }
        }
    }
    ICModule {
        id: ui
        name: "ui"
        subfolder: "ui/"
        pubincludes: "include/"
        ICLibrary {
            name: "icdatamodel"
            incpath: includes
            srcpath: subfolder+"icdatamodel/"
            files: [
                srcpath+"icdatamodel.cpp",
                incpath+"icdatamodel.h",
                incpath+"icdatamodel_global.h"
            ]
            cpp.includePaths: [includes, core.includes, tmpi.includes, containers.includes, database.includes]
            cpp.defines: ["ICDATAMODEL_LIBRARY"]
            Depends{ name: "Qt"; submodules: "qml"}
            Depends { name: "icobject" }
            Depends { name: "iccontainers" }
            Depends { name: "icdbrequests" }
            Depends { name: "icguiplugininterface" }
            Depends{ name: "icobjectrecord" }
            Depends{ name: "ichidb"}
        }
        ICLibrary {
            name: "icguiplugininterface"
            incpath: includes
            srcpath: subfolder+"icguiplugininterface/"
            files: [
                srcpath+"icguiplugininterface.cpp",
                incpath+"icguiplugininterface.h",
                incpath+"icguiplugininterface_global.h"
            ]
            cpp.defines: ["ICGUIPLUGININTERFACE_LIBRARY"]
            cpp.includePaths: [includes, core.includes, tmpi.includes]
            Depends { name: "ictmplugininterface" }
            Depends { name: "ivkplatform" }
            Depends{ name: "icobject"}
        }
        ICLibrary {
            name: "icsettingsmodel"
            incpath: includes
            srcpath: subfolder+"icsettingsmodel/"
            files: [
                srcpath+"*.cpp",
                incpath+"icsettingsmodel.h",
                incpath+"icsettingsmodel_global.h",
                incpath+"icsettingsnode.h",
                incpath+"icsettingsfiltermodel.h"
            ]
            cpp.includePaths: [includes, core.includes]
            cpp.defines: ["ICSETTINGSMODEL_LIBRARY"]
            Depends { name: "icobject" }
            Depends { name: "icsettings" }
            Depends { name: "ictreemodel" }
            //            Depends { name: "iccontainers" }
            //            Depends { name: "icdbrequests" }
            //            Depends { name: "icguiplugininterface" }
        }
        ICLibrary {
            name: "ictreemodel"
            incpath: includes
            srcpath: subfolder+"ictreemodel/"
            files: [
                incpath+"ictreemodel.h",
                incpath+"ictreenode.h",
                incpath+"ictreemodel_global.h",
                srcpath+"ictreemodel.cpp",
                srcpath+"ictreenode.cpp"
            ]
            cpp.defines: ["ICTREEMODEL_LIBRARY"]
            Depends { name: "icobject" }
            cpp.includePaths: base.concat(core.includes, incpath)
        }
        ICLibrary {
            id: iccontrols
            name: "iccontrols"
            srcpath: ui.subfolder+"iccontrols/"
            files: [
                srcpath+"*.cpp",
                srcpath+"*.h"
            ]
            Depends { name: "Qt"; submodules: ["quick","qml"]}
            Depends { name: "guiplugin"}
            Depends { name: "icguiplugininterface" }
            Depends { name: "ivkplatform" }
            Depends { name: "ictmplugininterface" }
            Depends { name: "iccontainers" }
            Depends { name: "icdatamodel" }
            Depends { name: "icsettings" }
            Depends { name: "icobject" }
            Depends { name: "icdbrequests" }
            Depends { name: "icobjectrecord" }
            Depends { name: "ichidb"}
            cpp.includePaths: [platform.coreInclude,platform.containersInclude,platform.uiInclude,platform.tmpiInclude,platform.dbInclude]
            installPath: "ivk/controls"
            Group {
                name: "Installation files"
                qbs.install: true
                qbs.installDir: "ivk/controls"
                files: srcpath+"qmldir"
            }
            Group {
                name: "QML"
                qbs.install: true
                qbs.installDir: "ivk/controls"
                files: srcpath+"qml/*"
            }
            Group {
                name: "Images"
                qbs.install: true
                qbs.installDir: "ivk/controls/images"
                files: srcpath+"qml/images/*"
            }
        }
    }
    Application {
        id: launcher
        name: "launcher"
        files: ["launcher/main.cpp"]
        destinationDirectory: "bin"
        Depends { name:"cpp" }
        Depends { name: "Qt"; submodules: ["core","network"] }
        Depends { name: "ivkplatform" }
        cpp.includePaths: qbs.targetOS.contains("windows")?[core.includes, "C:/Boost"]:[core.includes]
        cpp.cxxFlags: ['-std=c++0x']
        cpp.linkerFlags: ['-Wl,--no-undefined']
    }
    ICModule {
        id: plugins
        name: "plugins"
        subfolder: "plugins/"
        pubincludes: ""
        ICLibrary {
            name: "icsettingsclient"
            incpath: includes+name+"/"
            srcpath: subfolder+name+"/"
            installPath: "plugins"
            files: [
                srcpath+"icsettingsclient.cpp",
                srcpath+"icglobalsettings.cpp",
                incpath+"icsettingsclient.h",
                incpath+"icglobalsettings.h",
                incpath+"icsettingsclient_global.h"
            ]
            cpp.includePaths: [incpath, core.includes, tmpi.includes, containers.includes]
            cpp.defines: ["ICSETTINGSCLIENT_LIBRARY"]
            Depends { name: "ictmplugininterface" }
            Depends { name: "icsettings" }
            Depends { name: "iccontainers" }
            Depends { name: "icobject" }
            Depends { name: "ivkplatform" }
        }
        ICLibrary {
            name: "icsettingsserver"
            incpath: includes+name+"/"
            srcpath: subfolder+name+"/"
            installPath: "plugins"
            files: [
                srcpath+"icsettingsserver.cpp",
                incpath+"icsettingsserver.h",
                incpath+"icsettingsserver_global.h"
            ]
            cpp.defines: ["ICSETTINGSSERVER_LIBRARY"]
            cpp.includePaths: [incpath, core.includes, tmpi.includes, containers.includes]
            Depends { name: "ictmplugininterface" }
            Depends { name: "icsettings" }
            Depends { name: "iccontainers" }
            Depends { name: "icobject" }
            Depends { name: "ivkplatform" }
        }
//        ICLibrary {
//            name: "icluaplugin"
//            incpath: subfolder+name+"/"
//            srcpath: subfolder+name+"/"
//            installPath: "plugins"
//            files: [
//                srcpath+"*"
//            ]
//            cpp.defines: ["ICLUAPLUGIN_LIBRARY"]
//            cpp.includePaths: [incpath, core.includes, tmpi.includes, containers.includes]
//            Depends { name: "ictmplugininterface" }
//            Depends { name: "icsettings" }
//            Depends { name: "iccontainers" }
//            Depends { name: "icobject" }
//            Depends { name: "ivkplatform" }
//        }
    }
}
