#-------------------------------------------------
#
# Project created by QtCreator 2014-05-29T15:38:13
#
#-------------------------------------------------
include(../ui_common.pri)

QT       -= gui
CONFIG +=plugin

TARGET = icsettingsmodel
TEMPLATE = lib

DEFINES += ICSETTINGSMODEL_LIBRARY

SOURCES += icsettingsmodel.cpp \
    icsettingsnode.cpp \
    icsettingsfiltermodel.cpp

HEADERS +=$$UI_INCLUDE/icsettingsmodel.h \
    ../include/icsettingsnode.h \
    ../include/icsettingsfiltermodel.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -L$$DESTDIR -licobject -licsettings -lictreemodel
