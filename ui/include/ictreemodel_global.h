#ifndef ICTREEMODEL_GLOBAL_H
#define ICTREEMODEL_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ICTREEMODEL_LIBRARY)
#  define ICTREEMODELSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ICTREEMODELSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ICTREEMODEL_GLOBAL_H
