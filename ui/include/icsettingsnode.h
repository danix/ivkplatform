#ifndef ICSETTINGSNODE_H
#define ICSETTINGSNODE_H

#include <icsettingsmodel_global.h>
#include <ictreenode.h>
#include <QRegExp>

class ICSettingsModel;

/**
 * @brief Класс, описывающий атрибуты элемента дерева настроек
 * @ingroup settings gui
 */
class ICSETTINGSMODELSHARED_EXPORT ICSettingsNode : public ICObject, public ICTreeNode
{
    Q_OBJECT
public:
    //enums:
    /**
     * @brief Тип редактируемого значения
     */
    enum ICValueType
    {
        NoType,     ///< неопределен
        StringType, ///< строка
        NumType,    ///< число
        BoolType,   ///< флаг
        DateType,   ///< дата
        DateTimeType, ///< дата+время
        EnumType,    ///< перечисление
        PasswordType,///< пароль
    };
    Q_ENUMS(ICValueType)

    //constructors:
    explicit ICSettingsNode(QObject* parent=nullptr) : ICTreeNode(parent){}

    /**
     * @brief Создает элемент "настройка" дерева настроек
     * @param description название настройки
     * @param path путь к настройке (prefix/name)
     * @param type тип значения
     * @param global глобальная ли настройка?
     * @param parent предок
     */
    ICSettingsNode(ICSettingsModel* model,const QString& description,const QString& path,ICValueType type=StringType,QVariant defValue=QVariant(),bool advanced = false,bool local = true);
    virtual ~ICSettingsNode(){/*qDeleteAll(m_subnodes);*/}

    //properties:
    Q_PROPERTY(ICValueType valueType READ valueType WRITE setValueType NOTIFY valueTypeChanged)
    Q_PROPERTY(QVariant value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(QVariantList values READ values WRITE setValues NOTIFY valuesChanged) ///< "Список значений"

    //setters/getters:
    /**
    * @brief Возвращает путь (prefix/name)
    */
    inline QString path() const{return m_path;}
    /**
    * @brief Устанавливает путь (prefix/name)
    * @param val новое значение
    */
    inline void setPath(QString val){if(m_path!=val) m_path=val;}
    /**
    * @brief Возвращает значение настройки
    */
    inline QVariant value() const{return m_value;}
    /**
    * @brief Устанавливает значение настройки
    * @param val новое значение
    */
    virtual void setValue(QVariant val){if(!val.isValid() || m_value==val) return; m_value=val;emit valueChanged();}
    /**
    * @brief Возвращает тип значения настройки
    */
    inline ICValueType valueType() const{return m_valueType;}
    /**
    * @brief Устанавливает тип значения настройки
    * @param val новое значение
    */
    inline void setValueType(ICValueType val){if(m_valueType!=val) {m_valueType=val;emit valueTypeChanged();}}
    /**
    * @brief Возвращает значение свойства ""Список значений""
    */
    inline QVariantList values() const{return m_values;}
    /**
    * @brief Устанавливает значение свойства ""Список значений""
    * @param val новое значение
    */
    inline void setValues(QVariantList val){if(m_values==val) return; m_values=val; emit valuesChanged();}

    /**
     * @brief Оператор добавления дочернего элемента
     * @param node дочерний элемент
     */
    ICSettingsNode& operator <<(ICSettingsNode* node);
    /**
    * @brief Возвращает признак, является ли настройка дополнительной
    */
    inline bool advanced() const{return m_advanced;}
    /**
    * @brief Устанавливает признак, является ли настройка дополнительной
    * @param val новое значение
    */
    inline void setAdvanced(bool val){if(m_advanced!=val) m_advanced=val;}
    /**
     * @brief Удовлетворят ли настройка критериям фильтрации
     * @param rx регулярное выражение для фильтра названия
     * @param includeAdvanced включать дополнительные?
     * @return
     */
    bool match(const QRegExp& rx,bool includeAdvanced=true);
    /**
     * @brief Применить изменение для узла и всех его детей
     */
    virtual void apply();
    /**
     * @brief Сбросить значение настройки
     */
    void reset();

signals:
    void valueChanged();
    void valueTypeChanged();
    void valuesChanged();   ///< Сообщает об изменении значения свойства ""Список значений""

protected:
    bool event(QEvent *event) override;

    QVariantList m_values;
    QString m_settingName;
    bool m_local = true;
    QVariant m_defValue;
    QVariant m_syncValue;
private:
    void subscribe();

    // data members:
    QString m_path;
    ICValueType m_valueType = NoType;
    QVariant m_value;
    bool m_advanced = false;
};

/**
 * @brief Класс, описывающий категории настроек
 * @ingroup settings gui
 */
class ICSETTINGSMODELSHARED_EXPORT ICSettingsCategory : public ICSettingsNode
{
    Q_OBJECT
public:
    /**
     * @brief Создает элемент "категория" дерева настроек
     * @param description название
     * @param icon иконка
     */
    ICSettingsCategory(const QString& description,const QString& icon=QString(),bool advanced = false):
        ICSettingsNode()
    {
        setDescription(description);
        setIcon(icon);
        setAdvanced(advanced);
    }
};

#endif // ICSETTINGSNODE_H
