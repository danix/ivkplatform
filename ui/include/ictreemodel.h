#ifndef ICTREEMODEL_H
#define ICTREEMODEL_H

#include "ictreemodel_global.h"
#include <QAbstractItemModel>
#include <ictreenode.h>

/**
 * @brief Класс, описывающий абстрактную модель для редактирования настроек.
 * \ingroup settings gui
 */
class ICTREEMODELSHARED_EXPORT ICTreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    ICTreeModel(QObject* parent=0);
    virtual ~ICTreeModel(){/*qDeleteAll(m_nodes);*/}

    //enums
    enum TreeRoles
    {
        IconRole = Qt::UserRole+1,
        HasChildrenRole,
        DataRole
    };

    // overriden methods
    int columnCount(const QModelIndex&) const override{return 1;}
    int rowCount(const QModelIndex &parent) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    bool hasChildren(const QModelIndex &parent) const;

    /**
     * @brief Метод, выполняющий наполнение модели.
     * 
     * Должен быть переопределен в классе-наследнике.
     */
    virtual void load() = 0;

    /**
     * @brief Возвращает номер строки корневых элементов модели
     * @param node - указатель на элемент
     * @return номер строки переданного корневого элемента модели
     */
    inline int row(ICTreeNode* node) const {return m_nodes.indexOf(node);}

protected:
    /**
     * @brief Добавить элемент в корень дерева
     * @param node элемент
     */
    void append(ICTreeNode* node);
    /**
     * @brief Очистить дерево настроек
     */
    void clear(){qDeleteAll(m_nodes);m_nodes.clear();}

    // data members:
protected:
    QList<ICTreeNode*> m_nodes;
};

#endif // ICTREEMODEL_H
