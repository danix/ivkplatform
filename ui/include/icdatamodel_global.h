#ifndef ICDATAMODEL_GLOBAL_H
#define ICDATAMODEL_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ICDATAMODEL_LIBRARY)
#  define ICDATAMODELSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ICDATAMODELSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ICDATAMODEL_GLOBAL_H
