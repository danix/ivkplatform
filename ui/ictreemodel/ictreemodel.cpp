#include "ictreemodel.h"


ICTreeModel::ICTreeModel(QObject* parent)
    :QAbstractItemModel(parent)
{
}

int ICTreeModel::rowCount(const QModelIndex &parent) const
{
    if(!parent.isValid())
        return m_nodes.count();
    auto nd = (ICTreeNode*)parent.internalPointer();
    return nd->subnodes().count();
}

QModelIndex ICTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if(!parent.isValid())
    {
        if(row>=m_nodes.length())
            return QModelIndex();
        return createIndex(row,column,m_nodes.value(row));
    }
    auto node = (ICTreeNode*)parent.internalPointer();
    return createIndex(row,column,node->subnodes().value(row));
}

QModelIndex ICTreeModel::parent(const QModelIndex &child) const
{
    if(!child.isValid())
        return QModelIndex();
    auto nd = (ICTreeNode*)child.internalPointer();
    auto p = nd->parentNode();
    if (p == nullptr)
        return QModelIndex();
    auto parnode = dynamic_cast<ICTreeNode*>(p);
    if(parnode==nullptr)
        return QModelIndex();
    return createIndex(parnode->row(),0,parnode);
}

QVariant ICTreeModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    auto node = (ICTreeNode*)index.internalPointer();
    switch (role)
    {
    case Qt::DisplayRole:
        return node->description();
    case IconRole:
        return node->icon();
    case HasChildrenRole:
        return node->containsNodes();
    case DataRole:
        return QVariant::fromValue(node);
    default:
        break;
    }
    return QVariant();
}

QHash<int, QByteArray> ICTreeModel::roleNames() const
{
    return {
        {Qt::DisplayRole,"display"},
        {IconRole,"icon"},
        {HasChildrenRole,"hasChildren"}
//        {DataRole,"nodeData"}
    };
}

bool ICTreeModel::hasChildren(const QModelIndex &parent) const
{
    if(!parent.isValid())
        return m_nodes.count()>0;
    auto node = (ICTreeNode*)parent.internalPointer();
    return node->containsNodes();
}

void ICTreeModel::append(ICTreeNode *node)
{
    Q_ASSERT(node!=nullptr);
    // TODO: восстановить
//    node->setParent(this);
    node->setParentNode(nullptr);
    beginInsertRows(QModelIndex(),m_nodes.count(),m_nodes.count());
    m_nodes << node;
    endInsertRows();
}
