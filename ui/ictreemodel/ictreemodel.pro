#-------------------------------------------------
#
# Project created by QtCreator 2014-06-23T17:35:00
#
#-------------------------------------------------
include(../ui_common.pri)

QT       -= gui
CONFIG +=plugin

TARGET = ictreemodel
TEMPLATE = lib

DEFINES += ICTREEMODEL_LIBRARY

SOURCES += ictreemodel.cpp \
    ictreenode.cpp

HEADERS += $$UI_INCLUDE/ictreemodel.h \
    ../include/ictreenode.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -L$$DESTDIR -licobject
