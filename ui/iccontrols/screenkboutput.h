#ifndef SCREENKBOUTPUT_H
#define SCREENKBOUTPUT_H

#include <QObject>

class ScreenKbOutput : public QObject
{
    Q_OBJECT
public:
    ScreenKbOutput(){m_defLayout = 0;}
    Q_PROPERTY(int defaultLayout READ defaultLayout WRITE setDefaultLayout NOTIFY defaultLayoutChanged)

    inline int defaultLayout(){ return m_defLayout;}
public slots:
    void setDefaultLayout(int val){if(m_defLayout==val) return;m_defLayout = val;emit defaultLayoutChanged(val);}
signals:
    void charAppended(QString ch);
    void charRemoved();
    void defaultLayoutChanged(int val);
    void accepted();
private:
    int m_defLayout;
};

#endif // SCREENKBOUTPUT_H
