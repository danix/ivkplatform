QT += core

TARGET=luaplugin

TEMPLATE=lib

CONFIG += plugin

DEFINES += LUA_PLUGIN

PLATFORM_DIR = "../.."
PLATFORM_INC = $$PLATFORM_DIR/include
PLATFORM_SRC = $$PLATFORM_DIR/src

INCLUDEPATH += $$PLATFORM_DIR/include

HEADERS += luaplugin.h \
           luaplugin_global.h \
           $$PLATFORM_INC/icplugininterface.h \
           $$PLATFORM_INC/icplatformlayer.h \
           $$PLATFORM_INC/iclocalsettings.h

SOURCES += luaplugin.cpp \
           $$PLATFORM_SRC/icplatformlayer.cpp \
           $$PLATFORM_SRC/icplugininterface.cpp \ 
           $$PLATFORM_SRC/iclocalsettings.cpp

QMAKE_CXXFLAGS += -std=c++0x

QMAKE_LFLAGS += -Wl,--no-undefined

LIBS += -llua
