/**
 * @file luaplugin.h
 * @brief Класс, который может работать с плагинами на языке Lua
 * @copyright ivk-center.ru
 */

#ifndef LUAPLUGIN_H
#define LUAPLUGIN_H

#include <icplugininterface.h>
#include <lua.hpp>
#include "luaplugin_global.h"

class LUAPLUGINSHARED_EXPORT LuaPlugin: public ICPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ivk-center.luaplugin")
    Q_INTERFACES(ICPluginInterface)

public:
    LuaPlugin();
    virtual ~LuaPlugin();
    virtual ICPluginInfo pluginInfo();
				 
public slots:
    virtual void init(const QString& prefix);
    virtual void uninit();
		     
protected slots:
    virtual void processMessage(quint64 srcAddr, QByteArray data);
    virtual void onPluginsChanged();

protected:
    virtual void scriptInit();
    virtual void scriptUninit();
    virtual void scriptProcessMessage(quint64 srcAddr, QByteArray data);
    virtual void scriptOnPluginsChanged();
    lua_State* m_Lua;
};

#endif // LUAPLUGIN_H
