#-------------------------------------------------
#
# Project created by QtCreator 2014-03-26T09:11:57
#
#-------------------------------------------------
include(../plugins_common.pri)
QT       -= gui

TARGET = icsettingsclient
TEMPLATE = lib
CONFIG += plugin

DEFINES += ICSETTINGSCLIENT_LIBRARY

INCLUDEPATH+=$$PLATFORM_DIR/ictmplugininterface/include $$PLATFORM_DIR/containers/include

SOURCES += icsettingsclient.cpp \
           icglobalsettings.cpp

HEADERS += icsettingsclient.h \
           icglobalsettings.h

DESTDIR = $$PLATFORM_DIR/bin/plugins
LIBS += -L$$PLATFORM_DIR/bin -licsettings -licobject -livkplatform -lictmplugininterface -liccontainers
unix {
    target.path = /usr/lib
    INSTALLS += target
}
