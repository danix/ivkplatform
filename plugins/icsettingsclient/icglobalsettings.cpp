#include <icglobalsettings.h>
#include <icsettingsclient.h>

ICGlobalSettings::ICGlobalSettings()
{
    connect(ICSettingsClient::instance(),&ICSettingsClient::valueChanged,this,&ICGlobalSettings::onValueChanged);
}

void ICGlobalSettings::onValueChanged(QString name, QVariant value)
{
    if(!name.startsWith(m_prefix))
        return;
    name.remove(0,m_prefix.length());
    emit valueChanged(name,value);
}

QVariant ICGlobalSettings::value(const QString& key, const QVariant& defaultValue) const
{
    auto val = ICSettingsClient::instance()->read(m_prefix+key);
    if(val.isValid())
        return val;
    return defaultValue;
}

QVariant ICGlobalSettings::queryValue(const QString &name)
{
    return ICSettingsClient::instance()->beginRead(m_prefix+name);
}

void ICGlobalSettings::setValue(const QString& key, const QVariant& value)
{
    ICSettingsClient::instance()->write(m_prefix+key,value);
}

void ICGlobalSettings::beginGroup(const QString prefix)
{
    m_prefix = prefix+"/";
}

void ICGlobalSettings::endGroup()
{
    m_prefix.clear();
}

QStringList ICGlobalSettings::childGroups() const
{
    //TODO
    return QStringList();
}

QStringList ICGlobalSettings::childKeys() const
{
    //TODO
    return QStringList();
}

QStringList ICGlobalSettings::allKeys() const
{
    //TODO
    return QStringList();
}

void ICGlobalSettings::remove(const QString& key)
{
    //TODO
    Q_UNUSED(key);
}

void ICGlobalSettings::clear()
{
    //TODO
}
