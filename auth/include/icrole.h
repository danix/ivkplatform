#ifndef ICROLE_H
#define ICROLE_H

#include <icauth_global.h>
#include <icpermission.h>
#include <icrecordset.h>
#include <icobjectrecord.h>

/**
 * @brief Описание роли
 * @ingroup auth
 */
class ICAUTHSHARED_EXPORT ICRole : public ICObjectRecord
{
    Q_OBJECT
public:
    Q_PROPERTY ( QVariantList permissions READ vlPermissions WRITE setVlPermissions )
    Q_PROPERTY ( QVariantList roles READ vlRoles WRITE setVlRoles )

    /**
     * @brief Определяет политику вычисления суперпозиции прав доступа при конфликтах разрешений.
     */
    enum Policy
    {
        Allow,  ///< Разрешение преобладает над запрещением доступа.
        Decline ///< Запрещение преобладает над разрешением.
    };

    Q_INVOKABLE explicit ICRole(QObject* parent=nullptr);
    ICRole(const ICRole& cpy): ICRole(cpy.parent()){copy(&cpy);}
    virtual ~ICRole(){}
    void addRole(const ICRole& r);                      ///< Добавляет роль
    void delRole(const ICRole* r);                      ///< Удаляет роль

    void setRoles(const QList<ICRole*>& r) {m_roles=r;} ///< Устанавливает набор ролей
    const QList<ICRole*>& roles() const;                ///< Возвращает все роли

    void setVlRoles(const QVariantList& r);             ///< Только для свойства
    QVariantList vlRoles() const;                       ///< Только для свойства

    void addPermission(const ICPermission& p);       ///< Добавляет разрешение
    void delPermission(const ICPermission* p);       ///< Удаляет разрешение
    
    void setPermissions(const QList<ICPermission*>& p) {m_permissions = p;} ///< Устанавливает набор разрешений
    const QList<ICPermission*>& permissions() const;          ///< Возвращает список разрешений.
    
    void setVlPermissions(const QVariantList& p); ///< Устанавливает набор разрешений
    QVariantList vlPermissions() const;          ///< Возвращает список разрешений.

    /**
     * Возвращает набор разрешений данной роли, который является результатом суперпозиции всех разрешений данной роли
     * и разрешений вложенных ролей.
     */
    QList<ICPermission*> superposition(Policy superpositionPol, Policy accessPolicy) const;

    bool operator==(const ICRole& r);
    ICRole& operator<<(const ICRole& r);
    ICRole& operator<<(const ICPermission& r);

private:
    QList<ICPermission*> m_permissions; ///< набор разрешений
    QList<ICRole*> m_roles; ///< набор вложенных ролей
};

Q_DECLARE_METATYPE(ICRole)

#endif // ICROLE_H
