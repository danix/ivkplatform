#ifndef ICUSERINFO_H
#define ICUSERINFO_H

#include "icauth_global.h"
#include <icsecret.h>
#include <QDataStream>
#include <icrole.h>
#include <iccredentials.h>
#include <icobjectrecord.h>
#include <icrecord.h>
#include <icuserphoto.h>

/** 
 * @defgroup auth Аутентификация и система прав
 */

/**
 * @brief Класс представляющий информацию о пользователе.
 * @ingroup auth
 */
class ICAUTHSHARED_EXPORT ICUserInfo : public ICObjectRecord
{
    Q_OBJECT
public:
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged STORED true)
    Q_PROPERTY(QVariantList roles READ vlRoles WRITE setVlRoles STORED true)
    Q_PROPERTY(QString textSecret READ textSecret WRITE setTextSecret STORED false NOTIFY textSecretChanged)
    Q_PROPERTY(QString fullName READ fullName WRITE setFullName NOTIFY fullNameChanged STORED true)
    Q_PROPERTY(bool isBlocked READ isBlocked WRITE setIsBlocked STORED true)
    Q_PROPERTY(ICUserPhoto* userPhoto READ photo WRITE setPhoto NOTIFY userPhotoChanged STORED true)
    Q_PROPERTY(ICSecretContainer* secret READ secretMutable WRITE setSecret NOTIFY secretChanged STORED true)

    Q_INVOKABLE explicit ICUserInfo(QObject* parent=nullptr);
    ICUserInfo(const ICUserInfo& c);
//    ICUserInfo(const QString& name, const ICSecretContainer& secret, const ICRecordSet& roles, QObject* parent=nullptr):ICObjectRecord(parent){m_cred.setName(name); m_cred.setSecret(secret); m_roles = roles;}
    virtual ~ICUserInfo(){}
    QString name() const {return m_cred.name();}                               ///< Имя пользователя
    void setName(const QString& val){if(name()==val) return;m_cred.setName(val);emit nameChanged();}  ///< Устанавливает имя пользователя
    ICSecretContainer* secretMutable(){return m_cred.secretMutable();}
    const ICSecretContainer* secret() const {return m_cred.secret();}          ///< Бинарный контейнер с секретом пользователя
    void setSecret(const ICSecretContainer* secret){m_cred.setSecret(secret);emit secretChanged();} ///< устанавливает пароль пользователя
    QList<ICRole*> roles() const {return m_roles;}                             ///< Роли, которые назначены пользователю
    const QList<ICRole*>& constRoles() const {return m_roles;}                 ///< возвращает константную ссылку на роли
    void setRoles(const QList<ICRole*>& roles){m_roles=roles;}                 ///< устанавливает роли пользователя
    ICUserInfo& operator=(const ICUserInfo& i);                                ///< оператор присваивания

    QVariantList vlRoles() const;                                              ///< функция только для свойства
    void setVlRoles(const QVariantList &rs);                                   ///< функция только для свойства

    inline QString textSecret() const {return "";}                             ///< Отображение секрета строкой
    void setTextSecret(const QString & textSecret);                            ///< Задание секрета строкой

    inline QString fullName() const {return m_fullName;}                       ///< Возвращает полное имя
    inline void setFullName(const QString& name) {if(m_fullName==name) return; m_fullName = name;emit fullNameChanged();} ///< Задает полное имя

    inline bool isBlocked() const {return m_isBlocked;}                        ///< Возвращает статус блокировки пользователя
    inline void setIsBlocked(bool blocked) {m_isBlocked = blocked;}            ///< Задает статус блокировки пользователя

    inline  ICUserPhoto* photo() {return &m_photo;}                        ///< Возвращает фотографию
    void setPhoto(const ICUserPhoto* photo);          ///< Задает фотографию

    Q_INVOKABLE bool loadPhoto(const QString &url);
signals:
    void nameChanged();
    void secretChanged();
    void textSecretChanged();
    void userPhotoChanged();
    void fullNameChanged();
protected:
    ICCredentials m_cred;   ///< аутентификационные данные
    QList<ICRole*> m_roles; ///< роли пользователя
    QString m_fullName;     ///< Ф.И.О. пользователя
    bool m_isBlocked=false; ///< заблокирован ли пользователь
    ICUserPhoto m_photo;    ///< фотография пользователя
};

Q_DECLARE_METATYPE(ICUserInfo)

#endif // ICUSERINFO_H
