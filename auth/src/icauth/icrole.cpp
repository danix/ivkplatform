#include <icrole.h>

IC_REGISTER_METATYPE(ICRole)
IC_REGISTER_ICRECORDTYPE(ICRole)

ICRole::ICRole(QObject *parent):ICObjectRecord(parent)
{
}

void ICRole::addRole(const ICRole& r)
{
    m_roles.append(new ICRole(r));
}

void ICRole::delRole(const ICRole* r)
{
    m_roles.removeAll((ICRole*)r);
}

ICRole& ICRole::operator<<(const ICRole& r)
{
    addRole(r);
    return *this;
}

const QList<ICRole*>& ICRole::roles() const
{
    return m_roles;
}

void ICRole::setVlRoles(const QVariantList &r)
{
    setFromVl<ICRole*>(r, &m_roles);
}

QVariantList ICRole::vlRoles() const
{
    return toVl<ICRole*>(m_roles);
}

void ICRole::addPermission(const ICPermission& p)
{
    m_permissions.append(dynamic_cast<ICPermission*>(p.makeCopy()));
}

ICRole& ICRole::operator<<(const ICPermission& r)
{
    addPermission(r);
    return *this;
}

void ICRole::delPermission(const ICPermission* p)
{
    m_permissions.removeAll((ICPermission*)p);
}

const QList<ICPermission*> &ICRole::permissions() const
{
    return m_permissions;
}

void ICRole::setVlPermissions(const QVariantList &p)
{
    setFromVl(p,&m_permissions);
}

QVariantList ICRole::vlPermissions() const
{
    return toVl<ICPermission*>(m_permissions);
}

bool ICRole::operator==(const ICRole& r)
{
    return m_permissions == r.m_permissions && m_roles == r.m_roles;
}

QList<ICPermission*> ICRole::superposition(Policy superpositionPol, Policy accessPolicy) const
{
    // сначала соберем все суперпозиции с вложенных ролей
    QList<ICPermission*> perm;
    for(auto r : m_roles)
    {
        auto role = dynamic_cast<ICRole*>(r);
        perm.append(role->superposition(superpositionPol, accessPolicy));
    }
    // добавим правила
    perm.append(m_permissions);
    // теперь посчитаем результат
    auto it = perm.begin();
    ICPermission *pc, *pn;
    while(it != perm.end())
    {
        auto n = it+1;
        pc=dynamic_cast<ICPermission*>(*it);
        pn=dynamic_cast<ICPermission*>(*n);
        while(n != perm.end())
        {
            if(pn->object() == pc->object()) // встретили правило для такого же объекта
            {
                if(pn->rule() == pc->rule()) // правило одно и то же
                {
                    pc->setPrivileges(pc->privileges() | pn->privileges());      // просто складываем типы доступа
                    delete pn;
                    n = perm.erase(n); // удалим это правило
                    continue;
                }
                else // правила разные
                {
                    auto p = pc->privileges() & pn->privileges(); // найдем пересечение
                    if(superpositionPol == Allow) // приоритет разрешений
                    {
                        if(pc->rule()==ICPermission::Access) // сбросим биты у того, у кого запрещение
                            pn->setPrivileges(pn->privileges() & p);
                        else
                            pc->setPrivileges(pc->privileges() & p);
                    }
                    else // приоритет запрета
                    {
                        if(pc->rule()==ICPermission::Access) // сбросим биты у того, у кого разрешение
                            pc->setPrivileges(pc->privileges() & p);
                        else
                            pn->setPrivileges(pn->privileges() & p);
                    }
                }
            }
            ++n;
        }
        if((pc->rule() == ICPermission::Deny) && (accessPolicy == Decline))
            it = perm.erase(it); // т.к. считается, что запрещено все, что не разрешено
        else if((pc->rule() == ICPermission::Access) && (accessPolicy == Allow))
            it = perm.erase(it); // т.к. считается, что разрешено все, что не запрещено
        else
            ++it;
    }
    return perm;
}
