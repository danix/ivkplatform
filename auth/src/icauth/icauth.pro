#-------------------------------------------------
#
# Project created by QtCreator 2014-03-27T10:34:02
#
#-------------------------------------------------
include(../../auth_common.pri)

QT       -= gui

TARGET = icauth
TEMPLATE = lib
CONFIG += plugin

DEFINES += ICAUTH_LIBRARY

SOURCES += \
    icpasswdsecret.cpp \
    icrole.cpp \
    icsession.cpp \
    iccredentials.cpp \
    icsecret.cpp \
    icsecretcontainer.cpp

HEADERS +=\
    $$AUTHINC/icauth_global.h \
    $$AUTHINC/icsecret.h \
    $$AUTHINC/icuserinfo.h \
    $$AUTHINC/icpasswdsecret.h \
    $$AUTHINC/icrole.h \
    $$AUTHINC/icpermission.h \
    $$AUTHINC/icsession.h \
    $$AUTHINC/icauthrequest.h \
    $$AUTHINC/icsecretcontainer.h \
    $$AUTHINC/iccredentials.h

INCLUDEPATH += $$AUTHINC $$PLATFORM_DIR/containers/include

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS+=-L$$DESTDIR -licobject -licobjectrecord
