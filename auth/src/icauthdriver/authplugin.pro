include(../plugins_common.pri)

#QT       -= gui

TARGET = authplugin
TEMPLATE = lib
CONFIG += plugin

DEFINES += ICAUTHPLUGINLIBRARY

SOURCES += icauthplugin.cpp

HEADERS += icauthplugin.h \
           icauthplugin_global.h \
    icauthcommandstatuses.h

INCLUDEPATH += ./include \
               $$PLATFORM_DIR/ictmplugininterface/include \
               $$PLATFORM_DIR/database/include \
               $$PLATFORM_DIR/iccontainers/include \
               $$PLATFORM_DIR/icauth/include

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS+= -L$$DESTDIR -licobject -lictmplugininterface -liccontainers -licdbrequests \
       -licdbconnection -livkplatform -licauth

DESTDIR = $$PLATFORM_DIR/bin/plugins
