#ifndef ICPASSWDCOMPARER_H
#define ICPASSWDCOMPARER_H

#include "icseccompareinterface.h"

/**
 * @brief Класс для сравнения секретов типа ICPasswdSecret.
 * @ingroup auth
 */
class ICPasswdComparer : public ICSecCompareInterface
{
public:
    ICPasswdComparer(QObject* parent=nullptr):ICSecCompareInterface(parent){}
    bool compare(const ICSecretContainer &query, const ICSecretContainer &etalon) const override;
};

#endif //ICPASSWDCOMPARER_H
