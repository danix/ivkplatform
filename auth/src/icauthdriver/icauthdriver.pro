include(../../auth_common.pri)

QT       -= gui

TARGET = authdriver
TEMPLATE = lib
CONFIG += plugin

DEFINES += ICAUTHDRIVERLIBRARY

SOURCES += icauthdriver.cpp \
           icpasswdcomparer.cpp

HEADERS += icauthdriver.h \
           icauthdriver_global.h \
           icpasswdcomparer.h \
           icseccompareinterface.h\
    $$AUTHINC/icauthcommandstatuses.h

INCLUDEPATH += $$AUTHINC \
               $$PLATFORM_DIR/ictmplugininterface/include \
               $$PLATFORM_DIR/database/include \
               $$PLATFORM_DIR/containers/include

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS+= -L$$DESTDIR -licobject -lictmplugininterface -liccontainers -licdbrequests \
       -licdbconnection -livkplatform -licauth -licdatadriverinterface -licobject -licobjectrecord

DESTDIR = $$PLATFORM_DIR/bin
