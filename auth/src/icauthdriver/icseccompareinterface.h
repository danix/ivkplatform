#ifndef ICSECCOMPAREINTERFACE_H
#define ICSECCOMPAREINTERFACE_H

#include <icobject.h>
#include <icsecret.h>

/**
 * @brief ICSecCompareInterface является базовым классом для написания классов сравнения ICSecret.
 *
 * Наследование от ICObject нужно для использования настроек.
 * @ingroup auth
 */
class ICSecCompareInterface : public ICObject
{
public:
    ICSecCompareInterface(QObject *parent = nullptr) : ICObject(parent){}
    virtual bool compare(const ICSecretContainer& query, const ICSecretContainer& etalon) const = 0;
};

#endif //ICSECCOMPAREINTERFACE_H
