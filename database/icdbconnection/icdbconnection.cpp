#include "icdbconnection.h"
#include <icsettings.h>
#include <icvaluemap.h>
#include <icobjectrecord.h>
#include <tuple>

/**
 * @brief Класс для именованного представления любого класса в виде пары ключ->значение.
 * @ingroup database
 */
class NamedValueMap : public ICValueMap
{
public:
    QString className() const override {return m_className;}
    void setClassName(const QString& cn) {m_className=cn;}
    ICRecord* newInstance() const override {return new NamedValueMap;}

private:
    QString m_className;
};

ICDbConnection::ICDbConnection(QObject *parent) : ICObject(parent)
{}

ICDbConnection::ICDbConnection(const QString& dbname, const QString& connectionString, QObject* parent)
    : ICObject(parent),
      m_dbname(dbname),
      m_connectionString(connectionString),
      m_state(Closed),
      m_dbInfo(nullptr)
{
    setConnectionString(connectionString);
}

bool ICDbConnection::open()
{
    // проверить инициализирована ли структура БД
    bool containsInfo = ICDbInformation::containsInformation(m_dbname);
    m_dbInfo = ICDbInformation::instance(m_dbname);
    // открыть подключение
    if(!openDB())
        return false;
    // если нет, то инициализировать
    if(!containsInfo)
    {
        auto types = queryTypes();
        for(auto type : types)
        {
            auto typeFields = queryFields(type);
            m_dbInfo->addType(type,typeFields);
        }
    }
    setState(Opened);
    return true;
}

void ICDbConnection::close()
{
    closeDB();
    setState(Closed);
}

bool ICDbConnection::beginTransaction(bool sameRequestType)
{
    if(m_isTransactionOpened)
    {
        reportError(trUtf8("Не удалось начать новую транзакцию, т.к. текущая транзакция не завершена."));
        return false;
    }
    m_isTransactionOpened = beginDBTransaction();
    m_sameRequestType = sameRequestType;
    m_isRequestReady = false;
    return m_isTransactionOpened;
}

bool ICDbConnection::endTransaction(bool toCommit)
{
    if(!endDBTransaction(toCommit))
        return false;
    m_isTransactionOpened = m_isRequestReady = m_sameRequestType = false;
    return true;
}

bool ICDbConnection::insert(const ICDbInsertRequest &request, ICDbResponse& response, int bulkSize)
{
    //bool ok = true;
    if(request.inserts.records.count()==0)
        return true;
    // проверить состояние подключения
    if(state()!=Opened)
    {
        reportError(trUtf8("Не удалось добавить объект, т.к. подключение не установлено"));
        return false;
    }
    // проверить, существует ли тип
    auto typeName = request.typeName();
    if(!m_dbInfo->containsType(typeName))
    {
        ICRecord* firstRec = request.inserts.records.first();
        if(!createTypesTree(firstRec, typeName))
        {
            reportError(trUtf8("%1:%2: Не удалось создать дерево типов для %3.").arg(__FILE__).arg(__LINE__).arg(typeName));
            return false;
        }
    }
    return recursiveInsert(request.typeName(), request.inserts.records, bulkSize, response);
}

bool ICDbConnection::recursiveInsert(const QString& itypeName, QList<ICRecord*> items, int bulkSize, ICDbResponse& resp, bool relationTable)
{
    m_isRequestReady=false;
    QString typeName = itypeName;
    typeName.remove("*");
    bool ok = true;
    QList<ICRecord*> *inss = &items;
    QList<ICRecord*> einss;
    QMultiMap<int, std::tuple<QString,QString,QVariantList>> lists; // список имен таблиц отношений, имен полей и значений которые надо добавить уже после добавления самого объекта
    if(hasEmbeddedRecords(items.first()))
    {
        bulkSize = 1;
        int itnumber=0;
        for(auto r : items)
        {
            NamedValueMap* record = new NamedValueMap();
            record->setClassName(r->className());
            for(auto n : r->names())
            {
                QVariant v = r->value(n);
                QVariantList vl;
                if(v.type() == QVariant::List || !relationTable)
                {
                    QVariantList vl = makeList(v);
                    if(vl.isEmpty())
                    {
                        if(v.type()!=QVariant::List)
                            record->setValue(n, v);
                        continue;
                    }
                    QString strname = getTypeName(vl.first())+"_relation__"+n;
                    QStringList ltypes;
                    for(const QVariant& v:vl)
                    {
                        QString tn = getTypeName(v);
                        if(!ltypes.contains(tn))
                            ltypes.append(tn);
                    }
                    record->setValue(strname, ltypes.join(";"));
                    lists.insert(itnumber,std::tuple<QString,QString,QVariantList>(strname, n, vl));
                }
                else if(v.type() == QVariant::UserType)
                {
                    QString tn = getTypeName(v);
                    // сначала проверим, а может объект вычитан из базы и его добавлять не надо!
                    ICObjectRecord* obj = v.value<ICObjectRecord*>();
                    if(obj->rowid()>0)
                    {
                        record->setValue(tn+"_id__"+n, obj->rowid()); // объект уже есть в базе, потому просто запишем его rowid
                        // FIXME: здесь еще нужно сделать update кол-ва ссылок (refCounter) на этот объект
                        continue;
                    }
                    // делаем новый инсерт
                    QList<ICRecord*> inss;
                    inss.append(obj); // каст к ICRecord здесь не канает, т.к. он не наследуется от QObject
                    ICDbResponse insresp;
                    if(!recursiveInsert(tn, inss, 1, insresp))
                        return false;
                    record->setValue(tn+"_id__"+n, insresp.records.first()->value("rowid"));
                }
                else
                    record->setValue(n, v); // значит тип стандартный
            }
            einss.append(record);
            itnumber++;
        }
        inss = &einss;
        m_isRequestReady=false;
    }
    if(bulkSize == 0) // значит нужно объединить все инсерты в один
        bulkSize = inss->count();

    //  сделаем простой инсерт простых объектов
    if(!simpleInsert(typeName, *inss, bulkSize, resp))
        return false;

    // если вообще есть списки или сложные объекты в добавленных объектах
    if(!lists.isEmpty())
    {
        // нужно взять идентификаторы добавленных объектов и записать в таблицы со списками
        // берем один добавленный объект
        for(int i=0; i<resp.records.size(); i++)
        {
            // смотрим, какие в нем есть списки
            auto lsts = lists.values(i);
            for(auto list: lsts)
            {
                QVariantList vl = std::get<2>(list);
                if(vl.isEmpty()) // а может вообще список пустой, а мы мучаемся!
                    continue;
                // эхх, не пустой список... значит состряпаем объекты с нужным наполнением, а дальше пусть рекурсивный алгоритм сам разбирается!
                QList<ICRecord*> lst;
                for(auto vv: vl)
                {
                    auto rt = new NamedValueMap();
                    rt->setClassName(typeName+"_to_"+getTypeName(vv)+"_relation");
                    rt->setValue(typeName+"_id__parentid", resp.records[i]->value("rowid"));
                    rt->setValue("listElement", vv);
                    rt->setValue("parentFieldName", std::get<1>(list));
                    lst.append(rt);
                }
                ICDbResponse resp(true);
                recursiveInsert(lst.first()->className(), lst, lst.size(), resp, true);
            }
        }
    }
    return ok;
}

bool ICDbConnection::simpleInsert(const QString &typeName, QList<ICRecord *> &inss, int bulkSize, ICDbResponse &resp)
{
    bool ok=true;
    bool resetRequest = false;
    while(!inss.isEmpty())
    {
        if(!m_isRequestReady)
        {
            ok = createUnknownAttributes(typeName, inss.first());
            if(!ok)
            {
                REPORT_ERROR_IN_LINE(trUtf8("Ошибка создания неизвестных атрибутов!"));
                return false;
            }
        }
        if(inss.count()<bulkSize) // если кол-во элементов в инсерте не делится на кол-во объединяемых элементов нацело, то для хвоста придется готовить отдельный запрос
        {
            m_isRequestReady = false;
            resetRequest = true;
            bulkSize = inss.count();
        }
        // создадим список инсертов для объединения
        QList<ICRecord*> tinss;
        tinss.reserve(bulkSize);
        for(int i=0; i<bulkSize; i++)
            tinss.append(inss.takeFirst());
        ok = insertBulk(typeName, tinss, resp);
        if(resetRequest) // сбросим, чтобы следующий инсерт готовился заново (т.к. последним вставлялся хвост блока инсертов)
            m_isRequestReady = false;
        if(!ok)
        {
            REPORT_ERROR_IN_LINE(trUtf8("Вставка записей завершилась неудачей."));
            return false;
        }
    }
    return ok;
}

bool ICDbConnection::update(const ICDbUpdateRequest &request)
{
//   return false;
    const ICRecord* changes = request.changes();
    if(changes->changes().isEmpty())
        return true;
    // проверить запрос
    if(!valid(&request))
        return false;
    // надо проверить, является ли тип сложным и имеет ли среди списка обновляемых полей вложенный объект.
    // Если имеется вложенный объект среди обновляемых полей, то надо рекурсивно вызвать update для этого объекта, а родительский перепаковать
    // в ICValueMap и уже тогда обновить. 
    ICDbUpdateRequest newreq;
    const ICDbUpdateRequest* req=&request;
    NamedValueMap* record = nullptr;
    // FIXME: а необязательно это проверять, т.к. почти то же самое ниже делается. Но пока пусть так поживет.
    if(hasEmbeddedRecords(changes, false))
    {
        record = new NamedValueMap();
        record->setClassName(request.typeName());
        // для начала надо понять, есть ли какие-то объекты, удовлетворяющие тем условиям, которые заданы
        ICDbSelectRequest sreq(request.databaseName());
        sreq.setTypeName(request.typeName());
        sreq.setConditions(request.conditions());
        sreq.setCascade(false);
        sreq.setGenericContainer(true);
        // сформируем список выбираемых полей, т.к. нам нужны только сылки на таблицы отношений
        auto chs = changes->changes();
        ICDbResponse sresp(true);
        if(!select(sreq, sresp))
            return false;
        if(sresp.records.isEmpty())
            return true; // FIXME: точно true?
        // есть как минимум 1 объект, который нужно обновить
        // нам теперь надо сделать запросы в связанные таблицы с целью получения идентификаторов связанных объектов
        // после чего удалим исчезнувшие и добавим появившиеся
        ICDbSelectRequest rsr;
        rsr.setDatabaseName(request.databaseName());
        rsr.setCascade(true);
        rsr.setGenericContainer(true);
        for(const ICRecord* rec: sresp.records)
        {
            // для каждого обновляемого элемента надо запросить все списки и связанные объекты
            QMultiHash<QString,qulonglong> objectsForDeletion; // имя_таблицы->идентификатор
            QList<QPair<QString,QVariant>> objectsForInsertion; // имя_свойства,объект. По-умолчанию там лежат все объекты из измененных свойств.
            for(auto ch:chs) // пробежимся по изменениям
            {
                auto v = changes->value(ch);
                ICDbResponse rsresp(false);
                QMultiHash<QString, const ICRecord*> refobjects; // связанные объекты. имя_типа->запись_об_объекте
                if(v.type()==QVariant::UserType || v.type()==QVariant::List)
                {
                    // здесь надо распарсить соответствующее поле, чтоб узнать типы вложенных объектов
                    QStringList refTypes;
                    for(auto rn : rec->names())
                    {
                        if(!isTableReference(rn) && !isListReference(rn))
                            continue;
                        QStringList tf = parseReferenceField(rn);
                        if(tf[1] == ch)
                        {
                            refTypes=rec->value(rn).toString().split(";", QString::SkipEmptyParts);
                            break;
                        }
                    }
                    for(auto rt:refTypes)
                    {
                        rsr.setTypeName(request.typeName()+"_to_"+rt+"_relation");
                        if(!select(rsr, rsresp))
                        {
                            reportError("Внезапность случилась!!!");
                            return false;
                        }
                        for(const ICRecord* sr:rsresp.records)
                            refobjects.insert(rt, sr);
                    }
                }
                else
                {
                    record->setValue(ch, v);
                    continue; // а ничего не надо, это простой тип и он обновится позже
                }
                // здесь мы имеем список идентификаторов связанных объектов для конкретного поля обновляемого объекта
                // теперь надо найти различия у старого и нового списков.
                QVariantList fo;
                if(v.type()==QVariant::UserType)
                    fo.append(v);
                else
                    fo = v.toList();

                for(auto foi:fo)
                    objectsForInsertion.append(QPair<QString,QVariant>(ch, foi));

                QStringList reftypes = refobjects.uniqueKeys();
                for(auto reftype: reftypes)
                {
                    QList<const ICRecord*> vals = refobjects.values(reftype);
                    for(QVariant& fv:fo) // поищем объекты из нового значения поля, которые надо добавить
                    {
                        QString cn;
                        if(fv.type()==QVariant::UserType)
                            cn = fv.value<ICObjectRecord*>()->className();
                        else
                            cn = QString(fv.typeName()).remove("*");
                        if(cn != reftype)
                            continue;
                        bool found=false;
                        for(const ICRecord* val : vals)
                        {
                            QVariant varval = val->value("listElement");
                            if(getTypeName(varval) == cn)
                            {
                                if(fv.type()==QVariant::UserType)
                                {
                                    ICObjectRecord* v1 = fv.value<ICObjectRecord*>();
                                    ICObjectRecord* v2 = varval.value<ICObjectRecord*>();
                                    if(v1->rowid() == v2->rowid())
                                    {
                                        found=true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if(fv == varval)
                                    {
                                        found=true;
                                        break;
                                    }
                                }
                            }
                        }
                        if(found)
                            objectsForInsertion.removeOne(QPair<QString, QVariant>(ch, fv));
                    }
                    for(const ICRecord* p: vals) // поищем объекты из старого значения поля, которые надо удалить
                    {
                        bool found=false;
                        QVariant varp = p->value("listElement");
                        QString fieldName = p->value("parentFieldName").toString();
                        for(QVariant& fv:fo)
                        {
                            QString cn;
                            if(fv.type()==QVariant::UserType)
                                cn = fv.value<ICObjectRecord*>()->className();
                            else
                                cn = QString(fv.typeName()).remove("*");

                            if(cn==reftype)
                                if(ch==fieldName)
                                {
                                    if(fv.type()==QVariant::UserType)
                                    {
                                        ICObjectRecord* v1 = fv.value<ICObjectRecord*>();
                                        ICObjectRecord* v2 = varp.value<ICObjectRecord*>();
                                        if(v1->rowid()==v2->rowid())
                                        {
                                            found=true;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if(fv==varp)
                                        {
                                            found=true;
                                            break;
                                        }
                                    }
                                }
                        }
                        if(!found)
                        {
                            if(QVariant::nameToType(reftype.toUtf8().constData())==QVariant::UserType)
                            {
                                ICObjectRecord* v = varp.value<ICObjectRecord*>();
                                objectsForDeletion.insert(reftype, v->rowid());
                            }
                            else
                            {
                                objectsForDeletion.insert(request.typeName()+"_to_"+reftype+"relation", p->value("rowid").toULongLong());
                            }
                        }
                    }
                }
                // теперь у нас есть объекты, которые надо добавить и объекты, которые надо удалить
                // осталось только это все обработать
                // TODO: вместо тупого удаления надо сделать вычитание refCounter
                QStringList deltypes=objectsForDeletion.uniqueKeys();
                for(const QString& deltype:deltypes)
                {
                    // FIXME: для простых типов надо удалять из таблицы отношений
                    ICDbRemoveRequest rreq(request.databaseName());
                    rreq.setCascade(true);
                    rreq.setTypeName(deltype);
                    ICConditionSet cset;
                    auto ids = objectsForDeletion.values(deltype);
                    for(auto id:ids)
                        cset|=ICCondition("rowid", id);
                    rreq.setConditions(cset);
                    remove(rreq); // FIXME: обработать ошибку. А то погрязнем в мусоре.
                }
                // теперь добавим записи в таблицу отношений
                for(QPair<QString,QVariant>& pair: objectsForInsertion)
                {
                    // надо его добавить в таблицу отношений
                    QString ft = getTypeName(pair.second);
                    NamedValueMap nvm;
                    nvm.setClassName(request.typeName()+"_to_"+ft+"_relation");
                    nvm.setValue("parentFieldName", pair.first);
                    nvm.setValue(request.typeName()+"_id__parentid", rec->value("rowid"));
                    nvm.setValue("listElement", pair.second);
                    ICDbResponse iresp(true);
                    if(!m_dbInfo->containsType(nvm.className()))
                        if(!createTypesTree(&nvm,"",true))
                        {
                            reportError("Ошибка создания типа таблицы связей при обновлении элемента");
                            return false;
                        }
                    if(!recursiveInsert(nvm.className(), {&nvm}, 1, iresp, true))
                    {
                        reportError("Ошибка добавления в таблицу связей при обновлении элемента");
                        return false;
                    }
                    // тут еще надо у родительского объекта проверить, есть ли этот тип в списке типов в листе.
                    bool keyFound=false;
                    for(QString nm:rec->names())
                    {
                        if(nm.contains("_relation__"+pair.first)) // т.к. неизвестен базовый тип списка
                        {
                            QString reftypeslist = record->value(nm).toString();
                            if(reftypeslist=="")
                                reftypeslist = rec->value(nm).toString();
                            if(!reftypes.contains(ft))
                                reftypeslist = reftypeslist+";"+ft;
                            keyFound=true;
                            record->setValue(nm, reftypeslist);
                            break;
                        }
                    }
                    if(!keyFound)
                    {
                        // это значит, что такого поля нет в оригинальном объекте. А значит его надо добавить.
                        // но вот беда, какой же базовый ти у поля? А на самом деле пофиг, т.к. мы его нигде не используем :)
                        record->setValue(ft+"_relation__"+pair.first, ft);
                    }
                    // все добавили, радуемся пять минут.
                }
            }
        }
        qDeleteAll(sresp.records);
        sresp.records.clear();

        if(record->names().isEmpty()) // нет полей для обновления
        {
            delete record;
            return true; // FIXME: точно true? Или надо анализировать, было ли какое-то обновление вложенных объектов?
        }
        newreq.setDatabaseName(request.databaseName());
        newreq.setChanges(record);
        newreq.setConditions(request.conditions());
        newreq.setTypeName(request.typeName());
        req = &newreq;
    }
    // добавить неизвестные атрибуты
    if(!createUnknownAttributes(req->typeName(),req->changes()))
        return false;
    bool ret = updateObjects(*req);
    if(record)
        delete record;
    return ret;
}

bool ICDbConnection::remove(const ICDbRemoveRequest &request)
{
    // проверить запрос
    if(!valid(&request))
        return false;
    // Т.к. всегда используется отношение N:N и в каждом объекте храним кол-во ссылок на него от других объектов, тогда при удалении
    // дочернего объекта мы удаляем из таблицы отношений все ссылки на него и головной объект остается с пустым полем. При удалении головного
    // объекта нам придется проходить по соответствующей таблице отношений и смотреть, есть ли в ней ссылки от головного объекта на дочерние.
    // Если есть, то мы смотрим на эти объекты и если кол-во ссылок на них от других объектов = 1, то грохаем их.
    if(!request.cascade())
        return removeObjects(request);
    // здесь у нас есть проблема определения того, является ли объект сложным.
    // Мы это можем сделать тремя путями:
    // 1. Сделать выборку объектов по критериям их удаления, а затем (взяв первый попавшийся из выборки) определить сложность.
    // 2. Выбрать вообще первый попавшийся объект из нужной таблицы и определить сложность по нему.
    // 3. По имени таблицы создать такой объект и определить сложность по нему. Хотя такой вариант не канает, т.к. нам в перспективе все равно
    //    надо будет имена связанных таблиц, а их мы не сможем узнать, т.к. в свежем объекте списки будут пустые, а значит тип мы не узнаем.
    // Проблема в том, что, с одной стороны, запрос может быть сложным и мы сильно просядем в производительности. А сдругой стороны,
    // если объект окажется сложным, то нам все равно придется делать такую выборку (хотя не все поля, а только rowid).

    // За рабочий вариант примем такое решение: делаем выборку из таблицы по критериям удаления, определяем сложность этого типа, затем (если тип сложный)
    // составляем список связанных таблиц, делаем выборки из всех связанных таблиц с этими rowid
    // и рекурсивно дропаем нафиг все связанные объекты, если у них refCounter==1, или обновляем refCounter у них, уменьшая его на 1.

    ICDbSelectRequest sreq;
    sreq.setCascade(false);
    sreq.setGenericContainer(true); // нам надо имена связанных таблиц, потому так
    sreq.setDatabaseName(request.databaseName()); // для порядку!!!
    sreq.setTypeName(request.typeName());
    sreq.setConditions(request.conditions());
    ICDbResponse resp(false);
    if(!selectObjects(sreq, resp))
    {
        // TODO: как-то бы поаккуратнее обработать...
//            delete obj;
        reportError(QString("%1:%2 : Невозможно определить сложность типа %3 при рекурсивном удалении").arg(__FILE__).arg(__LINE__).arg(request.typeName()));
        return false;
    }
    if(resp.records.size()==0) // а таблица-то пустая!
        return true;
//    ICRecord* sample = resp.records.first();
//    resp.records.clear(); // на будущее
    QStringList referenceTables;
    ICConditionSet cset;
    for(const ICRecord* sample: resp.records)
    {
        auto fieldNames = sample->names();
        for(auto fname : fieldNames)
        {
            if(isListReference(fname))
            {
                //QStringList tf = parseReferenceField(fname);
                QStringList tns = sample->value(fname).toString().split(";",QString::SkipEmptyParts);
                for(auto tn:tns)
                {
                    QString refTableName = request.typeName()+"_to_"+tn+"_relation";
                    if(!referenceTables.contains(refTableName))
                        referenceTables.append(refTableName);
                }
            }
        }
        cset |= ICCondition(request.typeName()+"_id__parentid", sample->value("rowid"));
    }
    qDeleteAll(resp.records);
    resp.records.clear();
//    delete sample;
    if(referenceTables.isEmpty()) // а тип-то простой!!!
        return removeObjects(request);

    // здесь у нас есть идентификаторы всех объектов, которые будут удалены
    // FIXME: разбить единый пакет на меньшие согласно настройкам
    // Это надо, т.к. кол-во удаляемых может быть большое и БД вернет ошибку.
    QMultiHash<QString, qulonglong> forDeletion;
    sreq.setConditions(cset);
    sreq.setSelectionFields(ICDbSelectionFields());
    for(auto tableName : referenceTables)
    {
        ICDbResponse robjs(true);
        sreq.setTypeName(tableName);
        if(selectObjects(sreq, robjs))
        {
            for(const ICRecord* robj: robjs.records)
            {
                // надо из списка понять, является ли связанный объект сложным.
                // Если объект простой, то ничего и делать не надо.
                bool needContinue=false;
                auto nms=robj->names();
                for(auto nm : nms)
                {
                    if(isTableReference(nm))
                    {
                        auto tf = parseReferenceField(nm);
                        if(tf[1]!="listElement")
                            continue;
                        needContinue = true;
                        forDeletion.insert(tf[0], robj->value(nm).toULongLong());
                        break;
                    }
                }
                if(!needContinue)
                    break;
            }
        }
    }
    // здесь мы имеем хэш имен типов и идентификаторов объектов для рекурсивного удаления
    QStringList types = forDeletion.uniqueKeys();
    for(QString typeName:types)
    {
        // FIXME: здесь тоже надо запрос разбить на порции
        ICDbRemoveRequest rreq;
        rreq.setDatabaseName(request.databaseName()); // для порядку!!!
        rreq.setCascade(true);
        QList<qulonglong> ids = forDeletion.values(typeName);
        ICConditionSet cset;
        for(auto id:ids)
            cset |= ICCondition("rowid", id);
//        cset &= ICCondition("refCounter", 1); // FIXME: раскоментить, когда refCounter будет работать
        rreq.setConditions(cset);
        rreq.setTypeName(typeName);
        if(remove(rreq))
        {
            // удалили все объекты из таблицы, где refCounter==1
            // FIXME: для остальных надо сделать update c уменьшением refCounter
        }
    }
    // все, теперь можно со спокойной совестью удалять все остальное
    return removeObjects(request);
}

bool ICDbConnection::select(const ICDbSelectRequest &request, ICDbResponse &response)
{
    // проверить запрос
    if(!valid(&request))
        return false;
    m_sameRequestType = m_isRequestReady = false;
    // если объект простой (или у нас нерекурсивный селект), то надо сделать простой селект.
    // Если же объект сложный, то надо сделать выборку сначала основных объектов, а потом уже связанных.
    // При этом мы не разрешаем делать селект по признакам вложенного объекта. 
    // Для начала надо проверить, есть ли необходимость в рекурсивных селектах (т.е. проверить, есть ли в запрашиваемых типах вложенные объекты).
    // Если типы простые, то просто делаем нужное кол-во запросов в опрашиваемые таблицы, а затем приводим результаты к требуемому типу.
    // Если типы сложные, то запрашиваем объекты с упаковкой в ICValueMap, затем парсим поля со сложными типами и все перепаковываем.
    QStringList tt = request.privateTypes();
    if(tt.isEmpty())
        tt.append(request.typeName());
    bool ok = false;
    bool gcflag = request.genericContainer();
    for(auto tn : tt) // цикл по всем опрашиваемым таблицам
    {
        ICDbSelectRequest req = request;
        bool her = true;
        req.setTypeName(tn);
        if(!gcflag && request.cascade())
        {
            auto obj = ivk::icNewInstanceByName(tn);
            her = hasEmbeddedRecords(obj);
            delete obj;
        }
        if(her) // проверка на сложность типа
            req.setGenericContainer(true);
        ICDbResponse resp(false);
        if(!selectObjects(req, resp))
        {
            // TODO: как-то бы поаккуратнее обработать... 
//            delete obj;
            continue;
        }
        ok = true;
        if(her && request.cascade()) // теперь, если тип сложный, то распарсить имена полей со вложенными типами и сформировать запросы
        {
            for(const ICRecord* dbrecord : resp.records)
            {
                ICRecord* rec;
                if(!gcflag)
                    rec = ivk::icNewInstanceByName(tn);
                else
                    rec = new ICValueMap();
                for(auto nm : dbrecord->names())
                {
                    if(isTableReference(nm))
                    {
                        // надо сделать новый запрос к базе
                        ICDbSelectRequest sreq;
                        QStringList nv = parseReferenceField(nm);
                        if(nv[1]!="parentid") // если поле с таким именем, то для него не надо делать рекурсивный запрос, т.к. это таблица отношений.
                        {
                            sreq.setTypeName(nv[0]);
                            sreq.setCascade(true);
                            sreq.setConditions(ICConditionSet(ICCondition("rowid", dbrecord->value(nm), ICCondition::Equals)));
                            ICDbResponse r;
                            if(!select(sreq, r))
                            {
                                // TODO: аккуратнее надо быть, удалить всякое!
                                qDeleteAll(response.records);
                                response.records.clear();
                                return false;
                            }
                            if(!r.records.isEmpty())
                                rec->setValue(nv[1], QVariant::fromValue(dynamic_cast<ICObjectRecord*>(r.records[0])));
                        }
                        else
                            rec->setValue(nm, dbrecord->value(nm));
                    }
                    else if(isListReference(nm))
                    {
                        QStringList refTables = dbrecord->value(nm).toString().split(";", QString::SkipEmptyParts);
                        for(auto reftn:refTables)
                        {
                            ICDbSelectRequest sreq;
                            QStringList nv = parseReferenceField(nm);
                            sreq.setTypeName(tn+"_to_"+reftn+"_relation");
                            sreq.setCascade(true);
                            sreq.setGenericContainer(true);
                            ICConditionSet cset(ICCondition(tn+"_id__parentid", dbrecord->value("rowid"), ICCondition::Equals));
                            cset.append(ICCondition("parentFieldName", nv[1], ICCondition::Equals), ICLogicalPair::LO_AND);
                            sreq.setConditions(cset);
                            ICDbResponse sr;
                            if(!select(sreq, sr))
                            {
                                // TODO: аккуратнее надо быть, удалить всякое!
                                qDeleteAll(response.records);
                                response.records.clear();
                                return false;
                            }
                            QVariantList vl;
                            for(auto vli : sr.records)
                                vl.append(vli->value("listElement"));
                            // здесь надо различать реальный список и просто указатель на объект
                            if(rec->value(nv[1]).type()==QVariant::UserType)
                            {
                                if(!vl.isEmpty())
                                    rec->setValue(nv[1], vl.first());
                            }
                            else
                            {
                                QVariantList oldVals = rec->value(nv[1]).toList();
                                oldVals.append(vl);
                                rec->setValue(nv[1], oldVals);
                            }
                        }
                    }
                    else
                        rec->setValue(nm, dbrecord->value(nm));
                }
                response.records.append(rec);
            }
            qDeleteAll(resp.records);
        }
        else
            response.records.append(resp.records);  // а если тип простой, то надо просто результат пихнуть в ответ
//        delete obj;
    }
    return ok;
}

bool ICDbConnection::isTableReference(const QString& fieldName)
{
    return fieldName.contains(QRegExp("^.*_id__.*"));
}

bool ICDbConnection::isListReference(const QString& fieldName)
{
    return fieldName.contains(QRegExp("^.*_relation__.*"));
}

QStringList ICDbConnection::parseReferenceField(const QString& fieldName)
{
    QStringList sl = fieldName.split("__");
    QStringList sl1 = sl[0].split("_");
    return {sl1[0], sl[1]}; // {typeName, fieldName}
}

bool ICDbConnection::createUnknownAttributes(const QString& typeName,const ICRecord *rec)
{
    auto names = rec->names();
    ICDbTypeInformation* typeInfo = m_dbInfo->typeInfo(typeName);
    if(typeInfo==nullptr)
        return createTypesTree(rec, typeName);
    for(auto name: names)
    {
        if(typeInfo->containsField(name))
            continue;
        QVariant val = rec->value(name);
        QVariant::Type valType = val.type();
        if(valType == QVariant::UserType || (valType == QVariant::List && !val.toList().isEmpty()))
        {
            if(valType == QVariant::List && val.toList().isEmpty())
                continue;
            reportError(trUtf8("Мы напоролись на то, чего никогда не должно было случиться!!!"));
            return false;
        }
        ICDbFieldInfo fInfo {name, valType};
        if(!createField(typeName,fInfo))
            return false;
        typeInfo->addField(name);
    }
    return true;
}

bool ICDbConnection::checkState()
{
    if(state()!=Opened)
    {
        reportError(trUtf8("Не удалось выполнить запрос, т.к. подключение не установлено"));
        return false;
    }
    return true;
}

bool ICDbConnection::valid(const ICDbRequest *request)
{
    if(!checkState())
        return false;
    // проверить, существует ли тип
    bool ok = false;
    ok = m_dbInfo->containsType(request->typeName());
    auto selReq = dynamic_cast<const ICDbSelectRequest*>(request);
    if(selReq)
    {
        for(auto tp:selReq->privateTypes())
        {
            if(m_dbInfo->containsType(tp))
            {
                ok = true;
                break;
            }
        }
    }
    if(!ok)
        reportError(trUtf8("Не существует объектов данного типа: ")+request->typeName());
    return ok;
}

bool ICDbConnection::execute(const ICDbExecRequest&, ICDbResponse&)
{
    reportError(trUtf8("Операция не поддерживается бэкендом"));
    return false;
}

bool ICDbConnection::createTypesTree(const ICRecord* rec, const QString& typeAlias, bool relationTable)
{
    auto fieldNames = rec->names();
    QList<ICDbFieldInfo> fields;
    for(QString& fname : fieldNames)
    {
        QVariant val = rec->value(fname);
        // получим имя типа
        QString tn = val.typeName();
        tn.remove("*");

        QVariantList list;
        // узнаем тип содержимого
        if(val.type()==QVariant::List)
            list = val.value<QVariantList>();
        else if(val.type()==QVariant::UserType)
        {
            if(!relationTable)
                list.append(val);
            else
            {
                // проверим, возможно этот тип уже есть в базе
                QString tn = val.value<ICObjectRecord*>()->className();
                if(!m_dbInfo->containsType(tn)) // если типа нет, то
                {
                    // создадим объект этого типа (мы не берем его из реального объекта rec, т.к. там может быть нулевой указатель вместо экземпляра объекта)
                    ICRecord* intRec = ivk::icNewInstanceByName(tn);
                    if(intRec==nullptr)
                        return false;
                    // сделаем рекурсивный вызов
                    if(!createTypesTree(intRec))
                    {
                        delete intRec;
                        return false;
                    }
                    // удалим созданный нами эталонный объект
                    delete intRec;
                }
                QString fn = tn+"_id__"+fname;
                fields.append({fn, QVariant::ULongLong});
                fname = fn;
            }
        }
        else
            fields.append({fname, rec->value(fname).type()});

        if(!list.isEmpty())
        {
            QString tn = QString(list.first().typeName()).remove("*");
            QString resultTableName = typeAlias+"_to_"+tn+"_relation";
            auto rel = new NamedValueMap();
            rel->setClassName(resultTableName);
            rel->setValue("rowid", QVariant((qulonglong)0));
            rel->setValue(typeAlias+"_id__parentid", QVariant((qulonglong)0));
            rel->setValue("parentFieldName", fname);
            rel->setValue("listElement", list.first());
            if(!createTypesTree(rel, "", true))
            {
                reportError(trUtf8("%1:%2: Невозможно создать таблицу отношений для %3::%4").arg(__FILE__).arg(__LINE__).arg(rec->className()).arg(fname));
                return false;
            }
            delete rel;
            QString fn = tn+"_relation__"+fname;
            fields.append({fn, QVariant::String});
            fname = fn;
        }
    }
    // если не существует, добавить тип
    QString typeName = typeAlias;
    if(typeName=="")
        typeName = rec->className();
    if(!m_dbInfo->containsType(typeName)) // это если вдруг тип содержит список элементов такого же типа, то он уже был добавлен внутри рекурсии
    {
        if(!createObjectType(typeName, fields))
            return false;
        // и сохранить информацию о нем
        m_dbInfo->addType(typeName, fieldNames);
    }
    return true;
}

bool ICDbConnection::hasEmbeddedRecords(const ICRecord* rec, bool checkAllNames) const
{
    QStringList fieldNames;
    if(checkAllNames)
        fieldNames = rec->names();
    else
        fieldNames = rec->changes();
    for(auto fname : fieldNames)
    {
        QVariant val = rec->value(fname);
        if(val.type()==QVariant::List || val.type()==QVariant::UserType)
            return true;
    }
    return false;
}

QString ICDbConnection::getTypeName(const QVariant &val)
{
    QString cn;
    if(val.type()==QVariant::UserType)
        cn = val.value<ICObjectRecord*>()->className();
    else if(val.type()==QVariant::List)
    {
        QVariantList lst=val.toList();
        if(!lst.isEmpty())
            cn=QString(lst.first().typeName()).remove("*");
    }
    else
        cn = val.typeName();
    return cn;
}

QVariantList ICDbConnection::makeList(QVariant &v)
{
    if(v.type()==QVariant::List)
        return v.toList();
    else if(v.type()==QVariant::UserType)
    {
        QVariantList vl;
        vl.append(v);
        return vl;
    }
    return QVariantList();
}
