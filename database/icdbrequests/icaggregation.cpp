#include <icaggregation.h>

ICAggregation::ICAggregation()
{
}

QDataStream& operator << (QDataStream& stream, const ICAggregation& aggreg)
{
    stream<<aggreg.field<<(int)aggreg.func;
    return stream;
}

QDataStream& operator >> (QDataStream& stream, ICAggregation& aggreg)
{
    int f=0;
    stream >> aggreg.field;
    stream >> f;
    aggreg.func = (ICAggregation::AggregationFunc)f;
    return stream;
}
