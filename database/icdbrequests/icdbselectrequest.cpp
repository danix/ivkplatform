#include <icdbselectrequest.h>

void ICDbSelectRequest::serialize(QDataStream &output) const
{
    ICDbConditionRequest::serialize(output);
    int sd = (int)m_sortDir;
    output << m_count << m_offset << m_groupByField << m_selectionFields << m_sortByField << sd << privateTypes()<<m_onlyCount;
}

void ICDbSelectRequest::deserialize(QDataStream &input)
{
    ICDbConditionRequest::deserialize(input);
    int sd=0;
    input >> m_count >> m_offset >> m_groupByField >> m_selectionFields >> m_sortByField >> sd >> m_privateTypes>>m_onlyCount;
    m_sortDir = (SortingDirection)sd;
}

ICDbSelectRequest::ICDbSelectRequest(const ICDbSelectionFields& fields, const QStringList& from, const QString& groupBy, const ICConditionSet& conds, int count, int countFrom, ICSortingPair sorting, bool cascade)
    : ICDbConditionRequest("")
{
    m_selectionFields = fields;
    m_count=count;
    m_offset=countFrom;
    m_groupByField = groupBy;
    m_sortByField = sorting.first;
    m_sortDir = sorting.second;
    setCascade(cascade);
    setPrivateTypes(from);
    setConditions(conds);
}
