#include <icdbupdategrouprequest.h>


void ICDbUpdateGroupRequest::serialize(QDataStream &output) const
{
    output << m_requests.count();
    for(ICDbUpdateRequest req:m_requests)
        req.serialize(output);
}

void ICDbUpdateGroupRequest::deserialize(QDataStream &input)
{
    int cnt=0;
    input >> cnt;
    for(int i=0;i<cnt;i++)
    {
        ICDbUpdateRequest req;
        req.deserialize(input);
        m_requests << req;
    }
}
