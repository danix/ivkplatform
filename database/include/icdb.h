#ifndef ICDB_H
#define ICDB_H

#include "ichidb_global.h"
#include <ictmplugininterface.h>
#include <icdbresponse.h>
#include <icconditionset.h>
#include <icobjectrecord.h>
#include <icdbselectrequest.h>
#include <icdbupdategrouprequest.h>

/**
 * @brief Класс для работы с БД.
 *
 * Класс ,предоставляющий интерфейс для выполнения базовых операций с базой данных на верхнем уровне абстракции.
 * Элементарной единицой для всех операций являются наследники ICRecord.
 * @ingroup database
 *
 * @see ICObjectRecord
 */
class ICHIDBSHARED_EXPORT ICDb : public ICObject
{
    Q_OBJECT
public:
    explicit ICDb(ICTMPluginInterface* plugin,const QString& typeName=QString(),QObject* parent=0);
    /**
     * @brief Добавить список объектов в БД
     * @param records список объектов
     * @param operand тип операнда (кэшируется)
     */
    Q_INVOKABLE void insert(QList<ICRecord*> records,const QString& typeName=QString(),const QString& operandType=QString());
    /**
      * @brief Добавить объект
      * @param object добавляемый объект
      */
    Q_INVOKABLE void insert(ICObjectRecord* object);
    /**
     * @brief Изменить объект(ы)
     * @param changes измененные атрибуты
     * @param conds набор условий
     */
    Q_INVOKABLE void update(ICRecord* changes,const ICConditionSet& conds,const QString& typeName=QString());
    /**
     * @brief Обновить атрибуты объекта в БД
     * @param object обновляемый объект
     * @param cascade флаг каскадности
     */
    Q_INVOKABLE void update(ICObjectRecord* object,bool cascade=true);
    /**
     * @brief Удалить объект
     * @param object удаляемый объект
     * @param cascade флаг каскадного удаления
     */
    Q_INVOKABLE void remove(ICObjectRecord* object,bool cascade=true);
    /**
     * @brief Выборка объектов
     * @param typeName тип объекта
     * @param conds условие выборки
     * @param limit количество строк(по-умолчанию, все)
     * @param offset смещение от начала
     * @param orderBy имя поля, по которому нужно упорядочить
     * @param groupBy имя поля, по которому нужно сгруппировать
     */
    Q_INVOKABLE void select(const QString& typeName, const ICConditionSet& conds,int limit=0,int offset=0,const ICDbSelectionFields& selectionFields=ICDbSelectionFields(),
                            const ICDbSelectRequest::ICSortingPair& orderBy=ICDbSelectRequest::ICSortingPair(),const QString& groupBy=QString(),bool cascade=true);
    /**
     * @brief запросить из БД атрибуты объекта
     * @param object экземпляр объекта
     * @param cascade флаг каскадности
     */
    Q_INVOKABLE void select(ICObjectRecord* object,bool cascade=true);
    Q_INVOKABLE void count(const QString& typeName,const ICConditionSet& conds);
    inline void setOperandType(const QString& opType){m_operandType=opType;}
signals:
    /**
     * @brief Сигнал об успешном добавлении записей в БД
     * @param recs список добавленных объектов
     */
    void inserted(ICDbResponse resp);
    void updated(); ///< сигнал об успешном изменении объетка в БД
    /**
     * @brief Сигнал о завершении выборки
     * @param resp результат выборки
     */
    void selected(ICDbResponse resp);
private:
    void handleAnswer(quint64 addr,ICCommand& cmd);
    void handleAnswerByWaiter(quint64 addr,ICCommand& cmd,ICObjectRecord* waiter);
    ICCommand makeUpdateCommand(ICRecord* changes,const ICConditionSet& conds,const QString& typeName) const;
    void makeUpdateRequest(ICObjectRecord* record,ICDbUpdateGroupRequest& groupReq);
    QString m_typeName;
    QString m_operandType="icdbrequest";
    ICTMPluginInterface* m_plugin=nullptr;
    QSet<ICObjectRecord*> m_destroyedWaiters;
};

#endif // ICDB_H
