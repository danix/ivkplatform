#ifndef ICDBINSERTREQUEST_H
#define ICDBINSERTREQUEST_H

#include <icdbrequest.h>
#include <icrecordset.h>

/**
 * @brief Класс-контейнер запросов на добавление записей
 * @ingroup database
 */
class ICDBREQUESTSHARED_EXPORT ICDbInsertRequest : public ICDbRequest
{
public:
    ICDbInsertRequest(const QString& dbname="") : ICDbRequest(dbname){}

    ICRecordSet inserts;

    void serialize(QDataStream &output) const Q_DECL_FINAL;
    void deserialize(QDataStream &input) Q_DECL_FINAL;

    DbRequestType type() const Q_DECL_FINAL {return InsertType;}
    /**
     * @brief Добавляет к записям данного запроса записи операнда
     * @param req добавляемый запрос
     * @return true-успех, false-ошибка
     */
    bool append(ICDbInsertRequest* req);
};

#endif // ICDBINSERTREQUEST_H
