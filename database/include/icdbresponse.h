#ifndef ICDBRESPONSE_H
#define ICDBRESPONSE_H

#include <icrecordset.h>

/**
 * @brief Класс-контейнер для ответа на запросы к БД
 * @ingroup database
 */
typedef ICRecordSet ICDbResponse;

#endif // ICDBRESPONSE_H
