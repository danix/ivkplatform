#ifndef ICDATADRIVERINTERFACE_H
#define ICDATADRIVERINTERFACE_H

#include <icdatadriverinterface_global.h>
#include <icobject.h>
#include <iccommand.h>
#include <icdbconnpoolmanager.h>

/**
 * @brief Интерфейс для создания драйверов данных для ICDbServer
 * @see ICDbServer
 * @ingroup database
 */
class DDISHARED_EXPORT ICDataDriverInterface : public ICObject
{
    Q_OBJECT
public:
    typedef void (ICDataDriverInterface::*handlerFunc) (ICCommand&);
    ICDataDriverInterface(QObject *parent = 0);
    /**
     * @brief Функция инициализации модуля.
     * Здесь можно только прочитать локальные настройки,
     * зарегистрировать хэндлеры и не забыть вызвать init родительского класса.
     * @param prefix Префикс настроек.
     * @return Успех или нет.
     */
    virtual ICRETCODE init(const QString& prefix, ICDbConnPoolManager *poolManager);

    /**
     * @brief commandTypes Возвращает типы команд, обрабатываемые драйвером.
     */
    COMMAND_TYPE_LIST commandTypes();
public slots:
    void pushCommand(ICCommand cmd) {emit command(cmd);}
    void pushStart(){emit needStart();}

protected slots:
    /**
     * @brief onCommand Вызывается при поступлении команды драйверу.
     * @param cmd Команда.
     */
    void onCommand(ICCommand cmd);

    /**
     * @brief start Функция, которая сообщает драйверу о том, что можно произвести какую-то подготовительную работу.
     */
    virtual void start(){}

signals:
    void needStart(); /// сигнал, сообщающий о необходимости начала работы модуля. internal.
    void command(ICCommand cmd); /// сигнал, сообщающий, что пришла команда. internal.
    /**
     * @brief done Сигнализирует о том, что обработка команды закончена, надо послать ответ.
     * @param cmd команда-ответ.
     */
    void done(ICCommand cmd);
    /**
     * @brief send Отправляет команду без ожидания ответа всем, кто умеет ее обрабатывать.
     * @param cmd Команда.
     */
    void send(ICCommand cmd);

protected:
    /**
     * @brief Предоставляет хэндл подключения к БД для определенного запроса
     * @param request запрос
     * @return ICDbConnection*
     */
    virtual ICDbConnection* connection(const ICDbRequest* request);
    virtual void releaseConnection(ICDbConnection* con);
    /**
     * @brief Регистрирует функцию-обработчик команды заданного типа.
     * @param ct Тип обрабатываемой команды.
     * @param f Функция-обработчик.
     */
    void registerCommandHandler(CommandType ct, handlerFunc f);
    /**
     * @brief Объединяет запрос и ответ запроса на insert в ICDbInsertRequest
     * @param ins Запрос на insert
     * @param resp Ответ бэкенда на insert
     * @return ссылка на обновленный ICDbInsertRequest
     */
    ICDbInsertRequest& merge(ICDbInsertRequest& ins, const ICDbResponse& resp);
private:
    /**
     * @brief pool Возвращает пул подключений к базе с заданным именем.
     */
    ICDbConnectionPool* pool(const QString& dbname){return m_poolManager->pool(dbname);}

    QHash<CommandType, handlerFunc> m_pluginQueryHandlerByType; // Соответствие: тип обрабатываемой команды - обработчик запроса
    ICDbConnPoolManager* m_poolManager;
};

Q_DECLARE_INTERFACE(ICDataDriverInterface, "ivk-center.ICDataDriverInterface/1.0")

#endif // ICDATADRIVERINTERFACE_H
