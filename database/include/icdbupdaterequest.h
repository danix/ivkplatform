#ifndef ICDBUPDATEREQUEST_H
#define ICDBUPDATEREQUEST_H

#include <icdbconditionrequest.h>
#include <icrecordset.h>

/**
 * @brief Класс-контейнер запросов на изменения объектов
 * @ingroup database
 */
class ICDBREQUESTSHARED_EXPORT ICDbUpdateRequest : public ICDbConditionRequest
{
public:
    ICDbUpdateRequest(const QString& dbname="") : ICDbConditionRequest(dbname) {}
    ~ICDbUpdateRequest();
    DbRequestType type() const Q_DECL_FINAL{return UpdateType;}

    void serialize(QDataStream &output) const Q_DECL_FINAL;
    void deserialize(QDataStream &input) Q_DECL_FINAL;

    //setters/getters:

    inline const ICRecord* changes() const{return m_changes;}
    inline void setChanges(ICRecord* val){m_changes = val;}
    inline void allowDestroyRecords(bool toAllow){m_destroyRecords = toAllow;}

private:

    //members:

    bool m_destroyRecords = false;  ///< удалять записи в деструкторе контейнера?
    ICRecord* m_changes = nullptr;  ///< список записей с изменениями
};

#endif // ICDBUPDATEREQUEST_H
