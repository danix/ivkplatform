#ifndef ICDBEXECREQUEST_H
#define ICDBEXECREQUEST_H

#include <icdbrequest.h>

/**
 * @brief Класс-контейнер управляющего запроса к БД
 * @ingroup database
 */
class ICDBREQUESTSHARED_EXPORT ICDbExecRequest : public ICDbRequest
{
public:
    ICDbExecRequest(const QString& dbname) : ICDbRequest(dbname){}

    inline void setStmt(const QString& stmt){m_stmt=stmt;}
    inline const QString& stmt() const{return m_stmt;}

    void serialize(QDataStream &output) const Q_DECL_FINAL;
    void deserialize(QDataStream &input) Q_DECL_FINAL;

private:
    QString m_stmt; ///< строка запроса
};

#endif // ICDBEXECREQUEST_H
