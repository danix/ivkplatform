#ifndef ICDBSELECTREQUEST_H
#define ICDBSELECTREQUEST_H

#include <icdbconditionrequest.h>
#include <icrecord.h>
#include <icaggregation.h>
#include <QPair>

//class ICDbSelectRequest;
//enum ICDbSelectRequest::SortingDirection;

typedef ICAggregation ICDbSelectionField;
typedef ICAggregationSet ICDbSelectionFields;

/**
 * @brief Класс-контейнер запроса на выборку объектов из БД
 * @ingroup database
 */
class ICDBREQUESTSHARED_EXPORT ICDbSelectRequest : public ICDbConditionRequest
{
public:
    /**
     * @brief SortingDirection enum определяет направление сортировки данных
     */
    enum SortingDirection
    {
        ASC, ///< Сортировка по возрастанию
        DESC ///< Сортировка по убыванию
    };
    typedef QPair<QString, ICDbSelectRequest::SortingDirection> ICSortingPair;

    ICDbSelectRequest(const QString& dbname=""):ICDbConditionRequest(dbname){}
    /**
     * @brief Конструктор ICDbSelectRequest
     * @param fields поля для выборки. Если пусто, то выбирать все.
     * @param from список таблиц, из которых нужно делать выборку (это строковые имена типов объектов, которые нужно выбирать).
     * @param groupBy группировать по полю 
     * @param conds условия выборки
     * @param count кол-во объектов для выбора
     * @param countFrom с какого номера объекта считать кол-во выбираемых
     * @param sorting правила сортировки результатов
     * @param cascade нужно ли каскадно выбирать вложенные элементы сложных типов
     */
    ICDbSelectRequest(const ICDbSelectionFields& fields, const QStringList& from, const QString& groupBy, const ICConditionSet& conds, int count, int countFrom, ICSortingPair sorting, bool cascade);

    void serialize(QDataStream &output) const final;
    void deserialize(QDataStream &input) final;

    inline int count() const{return m_count;} ///< количество выбираемых элементов
    inline void setCount(int cnt){m_count = cnt;}
    inline int offset() const{return m_offset;} ///< номер записи, начиная с которой необходимо производить выборку
    inline void setOffset(int val){m_offset = val;}
    inline QString groupByField() const {return m_groupByField;} ///< группировать результаты по полю
    inline void setGroupByField(const QString& field){m_groupByField = field;}
    inline QString sortByField() const {return m_sortByField;} ///< сортировать данные по полю
    inline void setSortByField(const QString& field){m_sortByField = field;}
    inline SortingDirection sortingDirection() const {return m_sortDir;} ///< Направление сортировки
    inline void setSortingDirection(SortingDirection dir) {m_sortDir = dir;}
    inline void setSelectionFields(const ICDbSelectionFields& flds){m_selectionFields = flds;}
    inline const ICDbSelectionFields& selectionFields() const {return m_selectionFields;} ///< какие поля надо выбрать. Если пусто, то все.
    //members:
    inline bool onlyCount() const {return m_onlyCount;} ///< нужно только посчитать кол-во объектов, соответствующее запросу 
    inline void setOnlyCount(bool c) {m_onlyCount=c;if(c) setGenericContainer(true);}
    inline QStringList privateTypes() const {return m_privateTypes;} ///< типы, которые нужно реально запрашивать из базы. Здесь могут быть только те типы, которые в предках имеют тип в m_typeName
    void setPrivateTypes(const QStringList& tps) {m_privateTypes=tps;}

private:
    QStringList m_privateTypes; 
    int m_count=0;                      
    int m_offset=0;               
    QString m_groupByField;
    QString m_sortByField;                 
    ICDbSelectionFields m_selectionFields; 
    SortingDirection m_sortDir = ASC;      
    bool m_onlyCount=false;
};

#endif // ICDBSELECTREQUEST_H
