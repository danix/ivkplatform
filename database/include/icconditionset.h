#ifndef ICCONDITIONSET_H
#define ICCONDITIONSET_H

#include <iccondition.h>
#include <iclogicalpair.h>

/**
 * @brief Класс, представляющий собой набор условий, объединенных логическими операторами.
 * @ingroup database dataaccess gui
 */
class ICDBREQUESTSHARED_EXPORT ICConditionSet
{
public:
    ICConditionSet(){}
    ICConditionSet(const ICCondition& c);
    ICConditionSet(const ICConditionSet& cs){m_pairs = cs.m_pairs;}
    
    void append(const ICConditionSet& s, ICLogicalPair::LogicalOperator op); ///< добавление условия в конец списка через какой-либо логический оператор
    void clear() {m_pairs.clear();} ///< очищает набор условий 
    void operator &=(const ICCondition& c);
    void operator &=(const ICConditionSet& s);
    void operator |=(const ICCondition& c);
    void operator |=(const ICConditionSet& s);
    
    const QList<ICLogicalPair>& conditions() const {return m_pairs;} ///< возвращает ссылку на набор условий
    void setConditions(const QList<ICLogicalPair>& cs) {m_pairs=cs;}
    bool isEmpty(){return m_pairs.isEmpty();} ///< возвращает, пусто ли множество условий

    /**
     * @brief преобразовать в строковое представление
     * @return строка
     */
    QString toString() const;
    /**
     * @brief конструирует объект из текстового представления
     * @param expression строка вида: cond1 && cond2 || cond3...
     * @return
     */
    static ICConditionSet fromString(const QString& expression);
    inline bool isEmpty() const {return m_pairs.isEmpty();}
private:
    QList<ICLogicalPair> m_pairs;
};

ICDBREQUESTSHARED_EXPORT QDataStream& operator<<(QDataStream& out, const ICConditionSet& s);
ICDBREQUESTSHARED_EXPORT QDataStream& operator>>(QDataStream& in, ICConditionSet& s);
ICDBREQUESTSHARED_EXPORT ICConditionSet operator &&(const ICConditionSet& left,const ICConditionSet& right);
ICDBREQUESTSHARED_EXPORT ICConditionSet operator ||(const ICConditionSet& left,const ICConditionSet& right);
//ICConditionSet operator = (const ICConditionSet& copy)
//{
//    return ICConditionSet(copy);
//}

#endif // ICCONDITIONSET_H
