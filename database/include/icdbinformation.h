#ifndef ICDBINFORMATION_H
#define ICDBINFORMATION_H

#include <QHash>
#include <QStringList>

#ifdef IC_MTDB
    #include <QMutex>
    #include <QMutexLocker>
    #define IC_LOCK         QMutexLocker locker(&m_mutex); Q_UNUSED(locker);
    #define IC_DECL_MUTEX   private: \
                                QMutex m_mutex;
#else
    #define IC_LOCK
    #define IC_DECL_MUTEX
#endif

#define IC_DEFAULT_DBDRIVER     "QSQLITE"
#define IC_DEFAULT_DBHOSTNAME   "localhost"
#define IC_DEFAULT_DBPORT       -1
#define IC_DEFAULT_DBUSERNAME   ""
#define IC_DEFAULT_DBPASSWORD   ""


/**
 * @brief Класс, содержащий информацию о типе объекта БД
 * @ingroup database
 */
class ICDbTypeInformation
{
    IC_DECL_MUTEX
public:
    ICDbTypeInformation(const QStringList& fields) : m_fields(fields){}
    /**
     * @brief содержит атрибут с данным именем?
     * @param fname имя атрибута
     * @return да/нет
     */
    inline bool containsField(const QString& fname) const
    {
        return m_fields.contains(fname) || (m_fields.indexOf(QRegExp(".*_id__"+fname))!=-1) || (m_fields.indexOf(QRegExp(".*_relation__"+fname))!=-1);
    }
    /**
     * @brief добавить атрибут с данным именем
     * @param fname имя атрибута
     */
    inline void addField(const QString& fname)
    {
        IC_LOCK;
        m_fields << fname;
    }
private:
    QStringList m_fields;

};

/**
 * @brief Класс, содержащий информацию о структуре БД
 * @ingroup database
 */
class ICDbInformation
{
    IC_DECL_MUTEX
private:
    ICDbInformation(const QString& dbname);
public:
    ~ICDbInformation();
    /**
     * @brief содержит тип объекта с указанным именем?
     * @param typeName имя типа
     * @return
     */
    bool containsType(const QString& typeName) const;
    /**
     * @brief добавить информацию о типе объекта БД
     * @param typeName имя типа
     * @param fields список атрибутов
     */
    void addType(const QString& typeName,const QStringList& fields);
    /**
     * @brief запросить информацию о типе
     * @param typeName имя типа
     * @return
     */
    ICDbTypeInformation* typeInfo(const QString& typeName) const;
    /**
     * @brief получить инстанс класса по имени БД
     * @param dbname имя БД
     * @return инстанс
     */
    static ICDbInformation* instance(const QString& dbname=QString());
    /**
     * @brief содержит информацию о структуре БД
     * @param dbname имя БД
     * @return да/нет
     */
    inline static bool containsInformation(const QString& dbname){return m_databases.contains(dbname);}

    /**
     * @brief IP/имя сервера БД
     * @return
     */
    inline QString serverHostname() const {return m_serverHostname;}
    /**
     * @brief TCP-порт сервера БД
     * @return
     */
    inline int serverPort() const {return m_serverPort;}
    /**
     * @brief имя пользователя БД
     * @return
     */
    inline QString username() const {return m_username;}
    /**
     * @brief пароль пользователя БД
     * @return
     */
    inline QString password() const {return m_password;}
    /**
     * @brief имя драйвера БД
     * @return
     */
    inline QString driver() const {return m_driver;}
    /**
     * @brief название группы настроек для данной БД
     * @return имя группы настроек
     */
    inline QString settingsGroup() const {return m_settingsGroup;}
private:
    static QHash<QString,ICDbInformation*> m_databases;
    QHash<QString,ICDbTypeInformation*> m_types;
    QString m_serverHostname;
    int m_serverPort;
    QString m_username;
    QString m_password;
    QString m_driver;
    QString m_settingsGroup;
};

#endif // ICDBINFORMATION_H
