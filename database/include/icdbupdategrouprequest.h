#ifndef ICDBGROUPREQUEST_H
#define ICDBGROUPREQUEST_H

#include <icdbupdaterequest.h>

class ICDBREQUESTSHARED_EXPORT ICDbUpdateGroupRequest : public ICDbRequest
{
public:
    explicit ICDbUpdateGroupRequest(const QString& dbname="") : ICDbRequest(dbname){}
    // ICDbRequest interface
    void serialize(QDataStream &output) const;
    void deserialize(QDataStream &input);
    DbRequestType type() const{ return GroupUpdateType;}

    void operator <<(const ICDbUpdateRequest& req){m_requests << req;}
    inline const QList<ICDbUpdateRequest>& requests(){return m_requests;}
private:
    QList<ICDbUpdateRequest> m_requests;
};

#endif // ICDBGROUPREQUEST_H
