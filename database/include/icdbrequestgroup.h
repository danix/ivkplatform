#ifndef ICDBREQUESTSGROUP_H
#define ICDBREQUESTSGROUP_H

#include <icdbrequest.h>
#include <QList>

/**
 * @brief Контейнер группы
 * @ingroup database
 */
template<class T> class ICDbRequestGroup : public ICDbRequest
{
public:
    ICDbRequestGroup(const QString& dbname=QString()) : ICDbRequest(dbname){}

    /**
     * @brief Сериализует контейнер в бинарный массив
     * @return QByteArray
     */
    QByteArray toByteArray()
    {
        QByteArray result;
        QDataStream ds(&result,QIODevice::WriteOnly);
        ds << type();
        serialize(ds);
        return result;
    }
    void deserialize(QDataStream &input) Q_DECL_FINAL
    {
        ICDbRequest::deserialize(input);
        int cnt = 0;
        input >> cnt;
        for(int i=0;i<cnt;i++)
        {
            T req;
            req.deserialize(input);
            requests.append(req);
        }
    }

    QList<T> requests;   ///< запросы
private:
    DbRequestType type() const Q_DECL_FINAL{return requests.count()>0 ? requests[0].type() : CommonType;}

        void serialize(QDataStream &output) const Q_DECL_FINAL
        {
            ICDbRequest::serialize(output);
            output << requests.count();
            for(T req:requests)
                req.serialize(output);
        }
};

#endif // ICDBREQUESTSGROUP_H
