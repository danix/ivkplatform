#include <dbselect.h>
#include <ictmplugininterface.h>

ICDbSelectionFields& operator+(ICDbSelectionFields& first, const ICDbSelectionFields& second)
{
    first.append(second);
    return first;
}

ICDbSelectionFields makeSelectionFields(const QString& name, ICAggregation::AggregationFunc aggr)
{
    ICDbSelectionFields flds;
    if(name!="all")
        flds.append(ICDbSelectionField(aggr, name));
    return flds;
}

ICCommand& select(ICCommand& cmd, QString retType, const QStringList& fromTypes, const ICDbSelectionFields& fields,
                  const ICConditionSet& conds, int count, int countFrom, ICDbSelectRequest::ICSortingPair sort, bool cascade)
{
    ICDbSelectRequest req(fields, fromTypes, "", conds, count, countFrom, sort, cascade);
    cmd.type = COMMAND_TYPE_SELECT;
    cmd.object = retType;
    QDataStream str(&cmd.attrib, QIODevice::WriteOnly);
    req.serialize(str);
    return cmd;
}
