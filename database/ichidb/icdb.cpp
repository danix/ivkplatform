#include <icdb.h>
#include <icdballrequests.h>
#include <icdbresponse.h>
#include <QMetaMethod>
#include <functional>

#define IC_CHECK_ERROR(cmd)         {if(cmd.status==COMMAND_STATUS_ERROR) \
                                        { \
                                            ICError err(cmd.attrib); \
                                            reportError(trUtf8("Ошибка выполнения запроса БД: %1").arg(err.message())); \
                                            return; \
                                        } \
                                        if(cmd.status!=COMMAND_STATUS_OK) \
                                        { \
                                            qDebug()<<"ICDb::handleAnswer() recieves unexpected answer status: "<<cmd.status; \
                                            return; \
                                        }}


ICDb::ICDb(ICTMPluginInterface *plugin,const QString& typeName,QObject *parent)
    :ICObject(parent),
      m_typeName(typeName),
      m_plugin(plugin)
{
    Q_ASSERT(plugin);
}


void ICDb::insert(QList<ICRecord *> records,const QString& typeName, const QString& operandType)
{
    auto c= m_plugin->newCommand(COMMAND_TYPE_INSERT,operandType.isEmpty() ? m_operandType : operandType);
    QDataStream s(&(c.attrib), QIODevice::WriteOnly);
    ICDbInsertRequest ireq;
    ireq.setTypeName(typeName.isEmpty() ? m_typeName : typeName);
    ireq.inserts.records = records;
    ireq.serialize(s);
    auto m = QMetaMethod::fromSignal(&ICDb::inserted);
    ICTMPluginInterface::answerFunc cb = nullptr;
    if(isSignalConnected(m))
        cb = make_callback(&ICDb::handleAnswer);
    m_plugin->request(c,cb);
}

void ICDb::insert(ICObjectRecord *object)
{
    auto c = m_plugin->newCommand(COMMAND_TYPE_INSERT,m_operandType);
    QDataStream s(&(c.attrib), QIODevice::WriteOnly);
    ICDbInsertRequest ireq;
    ireq.setTypeName(object->className());
    ireq.inserts.records = {object};
    ireq.serialize(s);
    m_plugin->request(c,std::bind(&ICDb::handleAnswerByWaiter,this,std::placeholders::_1, std::placeholders::_2,object));
}

void ICDb::update(ICRecord *changes, const ICConditionSet &conds,const QString& typeName)
{
    auto c = makeUpdateCommand(changes,conds,typeName);
    auto m = QMetaMethod::fromSignal(&ICDb::updated);
    ICTMPluginInterface::answerFunc cb = nullptr;
    if(isSignalConnected(m))
        cb = make_callback(&ICDb::handleAnswer);
    m_plugin->request(c,cb);
}

void ICDb::update(ICObjectRecord *object,bool cascade)
{
    ICCommand cmd;
    if(cascade)
    {
        // сформировать групповой запрос
        ICDbUpdateGroupRequest groupReq;
        makeUpdateRequest(object,groupReq);
        cmd = m_plugin->newCommand(COMMAND_TYPE_UPDATE_GROUP,"icdbrequest");
        QDataStream s(&(cmd.attrib), QIODevice::WriteOnly);
        groupReq.serialize(s);
    }
    else
        cmd = makeUpdateCommand(object,ICCondition("rowid",object->rowid()),object->className());
    m_plugin->request(cmd,std::bind(&ICDb::handleAnswerByWaiter,this,std::placeholders::_1, std::placeholders::_2,object));
}

void ICDb::remove(ICObjectRecord *object, bool cascade)
{
    // сделать поддержку флага каскадности
    auto c = m_plugin->newCommand(COMMAND_TYPE_DELETE,m_operandType);
    ICDbRemoveRequest req;
    req.setCascade(cascade);
    req.setTypeName(object->className());
    req.setConditions(ICCondition("rowid",object->rowid()));
    QDataStream s(&(c.attrib), QIODevice::WriteOnly);
    req.serialize(s);
    m_plugin->request(c,std::bind(&ICDb::handleAnswerByWaiter,this,std::placeholders::_1, std::placeholders::_2,object));
}

void ICDb::select(const QString &typeName, const ICConditionSet &conds, int limit, int offset, const ICDbSelectionFields &selectionFields,
                  const ICDbSelectRequest::ICSortingPair &orderBy, const QString &groupBy,bool cascade)
{
    auto c = m_plugin->newCommand(COMMAND_TYPE_SELECT,m_operandType);
    QDataStream s(&(c.attrib), QIODevice::WriteOnly);
    ICDbSelectRequest req(selectionFields,QStringList(),groupBy,conds,limit,offset,orderBy,cascade);
    req.setTypeName(typeName.isEmpty() ? m_typeName : typeName);
    req.serialize(s);
    auto m = QMetaMethod::fromSignal(&ICDb::selected);
    ICTMPluginInterface::answerFunc cb = nullptr;
    if(isSignalConnected(m))
        cb = make_callback(&ICDb::handleAnswer);
    m_plugin->request(c,cb);
}

void ICDb::select(ICObjectRecord *object,bool cascade)
{
    auto c = m_plugin->newCommand(COMMAND_TYPE_SELECT,m_operandType);
    ICDbSelectRequest req;
    req.setCount(1);
    req.setCascade(cascade);
    req.setTypeName(object->className());
    if(object->rowid())
        req.setConditions(ICCondition("rowid",object->rowid()));
    QDataStream s(&(c.attrib), QIODevice::WriteOnly);
    req.serialize(s);
    connect(object,&QObject::destroyed,this,[=]{auto o = qobject_cast<ICObjectRecord*>(sender());m_destroyedWaiters<<o;});
    m_plugin->request(c,std::bind(&ICDb::handleAnswerByWaiter,this,std::placeholders::_1, std::placeholders::_2,object));
}

void ICDb::count(const QString &typeName, const ICConditionSet &conds)
{
    auto c = m_plugin->newCommand(COMMAND_TYPE_SELECT,m_operandType);
    QDataStream s(&(c.attrib), QIODevice::WriteOnly);
    ICDbSelectRequest req;
    req.setTypeName(typeName.isEmpty() ? m_typeName : typeName);
    req.setOnlyCount(true);
    req.setConditions(conds);
    req.serialize(s);
    auto m = QMetaMethod::fromSignal(&ICDb::selected);
    ICTMPluginInterface::answerFunc cb = nullptr;
    if(isSignalConnected(m))
        cb = make_callback(&ICDb::handleAnswer);
    m_plugin->request(c,cb);
}

void ICDb::handleAnswer(quint64 /*addr*/, ICCommand &cmd)
{
    IC_CHECK_ERROR(cmd);
    if(cmd.type==COMMAND_TYPE_UPDATE)
    {
        emit updated();
        return;
    }
    QDataStream input(cmd.attrib);
    ICDbResponse resp;
    input >> resp;
    switch(cmd.type)
    {
    case COMMAND_TYPE_INSERT:
        emit inserted(resp);
        break;
    case COMMAND_TYPE_SELECT:
    {
        for(auto r: resp.records)
            r->commit();
        emit selected(resp);
    }
        break;
    default:
        break;
    }
}

void ICDb::handleAnswerByWaiter(quint64 /*addr*/, ICCommand &cmd, ICObjectRecord *waiter)
{
    bool isWaiterAllive = !m_destroyedWaiters.contains(waiter);
    if(isWaiterAllive)
    {
        if(cmd.type == COMMAND_TYPE_UPDATE && cmd.status!=COMMAND_STATUS_OK)
            waiter->rollback();
    }
    IC_CHECK_ERROR(cmd);
    if(!isWaiterAllive)
    {
        m_destroyedWaiters.remove(waiter);
        return;
    }
    switch (cmd.type)
    {
    case COMMAND_TYPE_INSERT:
    {
        //FIXME: пока упрощенный вариант
        QDataStream input(cmd.attrib);
        ICDbResponse resp;
        input >> resp;
        if(!resp.records.isEmpty())
        {
            ICRecord* r = resp.records.first();
            if(r->contains("rowid"))
                waiter->setRowid(r->value("rowid").toULongLong());
            qDeleteAll(resp.records);
        }
        waiter->inserted();
    }
        break;
    case COMMAND_TYPE_UPDATE:
        waiter->commit();
        break;
    case COMMAND_TYPE_DELETE:
        emit waiter->removed();
        break;
    case COMMAND_TYPE_SELECT:
    {
        QDataStream input(cmd.attrib);
        ICDbResponse resp(true);
        input >> resp;
        if(resp.records.isEmpty())
            break;
        ICRecord* rec = resp.records.first();
        auto nms = rec->changes();
        for(auto nm: nms)
        {
            auto val = rec->value(nm);
            auto copyRec = [&](QVariant& v){
                ICObjectRecord* r = v.value<ICObjectRecord*>();
                Q_ASSERT(r);
                auto cpy= r->makeCopy();
                return QVariant::fromValue<QObject*>((ICObjectRecord*)cpy);
            };
            if(val.type()==QVariant::UserType)
                waiter->setValue(nm,copyRec(val));
            else if(val.type()==QVariant::List)
            {
                QVariantList copyList;
                for(auto el: val.toList())
                    copyList<<copyRec(el);
                waiter->setValue(nm,copyList);
            }
            else
                waiter->setValue(nm,val);
        }
        waiter->commit();
        emit waiter->selected();
    }
    default:
        break;
    }
}

ICCommand ICDb::makeUpdateCommand(ICRecord *changes, const ICConditionSet &conds,const QString& typeName) const
{
    auto cmd = m_plugin->newCommand(COMMAND_TYPE_UPDATE,m_operandType);
    QDataStream s(&(cmd.attrib), QIODevice::WriteOnly);
    ICDbUpdateRequest ureq;
    ureq.setTypeName(typeName.isEmpty() ? m_typeName : typeName);
    ureq.setConditions(conds);
    ureq.setChanges(changes);
    ureq.serialize(s);
    return cmd;
}

void ICDb::makeUpdateRequest(ICObjectRecord *record, ICDbUpdateGroupRequest &groupReq)
{
    //TODO:пробежаться по всем свойствам объекта и для сложных типов спуститься рекурсивно
    auto checkProp = [&](const QString& pname){
        auto checkVal = [&](QVariant& val){
            ICObjectRecord* objRec = val.value<ICObjectRecord*>();
            if(!objRec)
                return;
            if(objRec->rowid())
                makeUpdateRequest(objRec,groupReq);
        };
        auto propVal = record->property(pname.toUtf8().constData());
        switch (propVal.type()) {
        case QVariant::UserType:
            checkVal(propVal);
            break;
        case QVariant::List:
        {
            auto vl = propVal.toList();
            for(auto v: vl)
                checkVal(v);
        }
            break;
        default:
            break;
        }
    };
    int pcnt = record->metaObject()->propertyCount();
    for(int i=0;i<pcnt;i++)
        checkProp(record->metaObject()->property(i).name());
    for(auto dp: dynamicPropertyNames())
        checkProp(dp);
    // сериализовать запрос для текущего узла
    ICDbUpdateRequest req;
    req.setConditions(ICCondition("rowid",record->rowid()));
    req.setTypeName(record->className());
    req.setChanges(record);
    groupReq << req;
}
