#ifndef ICTESTPLUGIN_H
#define ICTESTPLUGIN_H


#include "ictestplugin_global.h"
#include "icplugininterface.h"

class ICTESTPLUGINSHARED_EXPORT ICTestPlugin: public ICPluginInterface
{
    Q_OBJECT
    Q_INTERFACES(ICPluginInterface)
    Q_PLUGIN_METADATA(IID "ivk-center.testplugin")
public:
    Q_INVOKABLE ICTestPlugin();
    ICPluginInfo pluginInfo()override;
    ICRETCODE init(const QString& pref)override;
    void uninit()override;
    void processMessage(quint64 srcAddr, const QByteArray& data);
protected slots:

    void onPluginsChanged();
    void start()override;

private:
    void enumerateNeighbours();
private:
    static int seed;
    ICPluginInfo info;
};

#endif // ICTESTPLUGIN_H
