#include "ictestplugin.h"
#include "QDebug"
#include "QtGlobal"

int ICTestPlugin::seed = 0;

ICTestPlugin::ICTestPlugin()
{
    qsrand(seed++);
    info.pluginName = QString::number(qrand());
    info.pluginVersion.major = 1;
    info.pluginVersion.minor = 6;
    info.pluginVersion.build = 4;
}

ICPluginInfo ICTestPlugin::pluginInfo()
{
    return info;
}

ICRETCODE ICTestPlugin::init(const QString& pref)
{
    return ICOK;
}

void ICTestPlugin::uninit()
{

}

void ICTestPlugin::processMessage(quint64 srcAddr, const QByteArray& data)
{
    qDebug()<<"Hooray, I got a message from "<<srcAddr;
}

void ICTestPlugin::start()
{
    qDebug()<<"Hooray, I just got started and my id is"<<thisPluginID();
    enumerateNeighbours();
}

void ICTestPlugin::onPluginsChanged()
{
    qDebug()<<"Remote plugins changed";
    enumerateNeighbours();
}

void ICTestPlugin::enumerateNeighbours()
{

    QList<quint64> pIDs = pluginIDs();
    for(quint64 id: pIDs)
    {
         qDebug()<<"Got a neighbour "<<id;
         sendMessage(id);
    }
}
