QT += testlib

PLATFORM_DIR = $$PWD/../..
PLATFORM_INC = $$PLATFORM_DIR/include
PLATFORM_SRC = $$PLATFORM_DIR/src

INCLUDEPATH += $$PLATFORM_DIR/include

HEADERS += localsettings.h \
           $$PLATFORM_INC/iclocalsettings.h \
           $$PLATFORM_INC/icsettings.h

SOURCES += localsettings.cpp \
           $$PLATFORM_SRC/icsettings/iclocalsettings.cpp
           $$PLATFORM_SRC/icsettings/icsettings.cpp
