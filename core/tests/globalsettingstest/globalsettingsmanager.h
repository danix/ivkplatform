#ifndef GLOBALSETTINGSMANAGER_H
#define GLOBALSETTINGSMANAGER_H

#include <icobject.h>

class GlobalSettingsManager : public ICObject
{
    Q_OBJECT
public:
    explicit GlobalSettingsManager(QObject *parent = 0);
public slots:
    void write();
};

#endif // GLOBALSETTINGSMANAGER_H
