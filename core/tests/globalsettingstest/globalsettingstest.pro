#-------------------------------------------------
#
# Project created by QtCreator 2014-03-26T16:27:20
#
#-------------------------------------------------
QT       += core network testlib
#QT       -= gui

TARGET = globalsettingstest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L$$PLATFORM_DIR/bin -licobject -licsettings -livkplatform \
#-licsettingsclient -lictmplugininterface \
    -liccontainers

SOURCES += main.cpp \
    globalsettingstest.cpp \
    globalsettingsmanager.cpp

HEADERS += \
    globalsettingstest.h \
    globalsettingsmanager.h

include(../icobjecttest/settingstest.pri)
INCLUDEPATH += $$SETTINGS_TESTDIR \
    $$PLATFORM_DIR/ictmplugininterface/include \
    $$PLATFORM_DIR/plugins/icsettingsclient/include
