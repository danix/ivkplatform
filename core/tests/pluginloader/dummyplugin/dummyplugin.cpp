#include "dummyplugin.h"
#include <QDebug>

ICPluginInfo DummyPlugin::pluginInfo()
{
    ICPluginInfo info;
    info.pluginName = "dummyplugin";
    info.pluginVersion = {0,1,0};
    info.platformCompat = {0,1,0};
    return info;
}

void DummyPlugin::processMessage(quint64 srcAddr, QByteArray data)
{
    qDebug()<<"Hello, dummy plugin!!!";
}

void DummyPlugin::init(QString& prefix)
{
    qDebug()<<"Dummy plugin init";
}

void DummyPlugin::uninit()
{
    ;
}
