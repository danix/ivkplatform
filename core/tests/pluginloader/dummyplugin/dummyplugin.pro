QT += core

TARGET=dummyplugin

TEMPLATE=lib

CONFIG += plugin

DEFINES += DUMMY_PLUGIN

PLATFORM_DIR = "../../.."
PLATFORM_INC = $$PLATFORM_DIR/include
PLATFORM_SRC = $$PLATFORM_DIR/src

INCLUDEPATH += $$PLATFORM_DIR/include

HEADERS += dummyplugin.h \
           dummyplugin_global.h \
           $$PLATFORM_INC/icplugininterface.h \
           $$PLATFORM_INC/icplatformlayer.h

SOURCES += dummyplugin.cpp \
           $$PLATFORM_SRC/icplatformlayer.cpp \
           $$PLATFORM_SRC/icplugininterface.cpp

QMAKE_CXXFLAGS += -std=c++0x

QMAKE_LFLAGS += -Wl,--no-undefined
