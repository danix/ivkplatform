QT += core network

TARGET=dummyplatform
TEMPLATE=app

PLATFORM_DIR = $$PWD/../..
PLATFORM_INC = $$PLATFORM_DIR/include
PLATFORM_SRC = $$PLATFORM_DIR/src

INCLUDEPATH += $$PLATFORM_DIR/include

HEADERS += myplatform.h \
           $$PLATFORM_INC/icplugininterface.h \
           $$PLATFORM_INC/icplatformlayer.h \
           $$PLATFORM_INC/icplatformloader.h \
           $$PLATFORM_INC/icpluginloader.h \
           $$PLATFORM_INC/icplugincontext.h \
    ../../include/icabstractnode.h \
    ../../include/icchipherencoder.h \
    ../../include/icclienthandshake.h \
    ../../include/iccompressencoder.h \
    ../../include/icconnection.h \
    ../../include/icconnectionfactory.h \
    ../../include/icdataencoder.h \
    ../../include/icdatastream.h \
    ../../include/icdiffiehellmankeygenerator.h \
    ../../include/ichandshake.h \
    ../../include/iciostream.h \
    ../../include/ickeygenerator.h \
    ../../include/iclocalnode.h \
    ../../include/iclocker.h \
    ../../include/icmessages.h \
    ../../include/icnetworkdetails.h \
    ../../include/icnetworkstream.h \
    ../../include/icnodeswitch.h \
    ../../include/icremotenode.h \
    ../../include/icremotenodesmanager.h \
    ../../include/icrouter.h \
    ../../include/icsecurestreamencoder.h \
    ../../include/icserverhandshake.h \
    ../../include/ictcpserver.h \
    ../../include/ictcpstream.h \
    ../../include/ictcpclientsloader.h

SOURCES += dummyplatform.cpp \
           $$PLATFORM_SRC/framework/plugininterfaces/icplatformlayer.cpp \
           $$PLATFORM_SRC/framework/misc/icplatformloader.cpp \
           $$PLATFORM_SRC/framework/misc/icpluginloader.cpp \
           $$PLATFORM_SRC/framework/plugininterfaces/icplugininterface.cpp \
    ../../src/framework/routing/icclienthandshake.cpp \
    ../../src/framework/routing/ichandshake.cpp \
    ../../src/framework/routing/iclocalnode.cpp \
    ../../src/framework/routing/iclocker.cpp \
    ../../src/framework/routing/icmessages.cpp \
    ../../src/framework/routing/icnetworkdetails.cpp \
    ../../src/framework/routing/icnodeswitch.cpp \
    ../../src/framework/routing/icremotenode.cpp \
    ../../src/framework/routing/icremotenodesmanager.cpp \
    ../../src/framework/routing/icrouter.cpp \
    ../../src/framework/routing/icserverhandshake.cpp \
    ../../src/framework/network/icchipherencoder.cpp \
    ../../src/framework/network/iccompressencoder.cpp \
    ../../src/framework/network/icconnection.cpp \
    ../../src/framework/network/icdataencoder.cpp \
    ../../src/framework/network/icdatastream.cpp \
    ../../src/framework/network/icdiffiehellmankeygenerator.cpp \
    ../../src/framework/network/iciostream.cpp \
    ../../src/framework/network/ickeygenerator.cpp \
    ../../src/framework/network/icnetworkstream.cpp \
    ../../src/framework/network/icsecurestreamencoder.cpp \
    ../../src/framework/network/ictcpclientsloader.cpp \
    ../../src/framework/network/ictcpserver.cpp \
    ../../src/framework/network/ictcpstream.cpp \
    ../../src/framework/network/sosemanuk.cpp

LIBS += -lqca -L$$PLATFORM_DIR/../bin -licobject -licsettings

OTHER_FILES += \
    ../../src/framework/network/network.pri
