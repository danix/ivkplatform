#-------------------------------------------------
#
# Project created by QtCreator 2014-03-20T10:44:26
#
#-------------------------------------------------

QT       += core testlib

#QT       -= gui

TARGET = icobjecttest
#CONFIG   += console
#CONFIG   -= app_bundle

TEMPLATE = app

include(settingstest.pri)

SOURCES += main.cpp

LIBS += -L$$PLATFORM_DIR/bin -licsettings -licobject

DEFINES += TEST_LOCAL
