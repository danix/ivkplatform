#ifndef ICMOCKNODE_H
#define ICMOCKNODE_H
#include "icabstractnode.h"
#include "gmock/gmock.h"

namespace icrouting {

class ICMockNode : public ICAbstractNode
{
public:

    MOCK_CONST_METHOD0(name, QString());

    MOCK_METHOD1(setName, void(QString name));

    MOCK_CONST_METHOD0(id, quint32());

    MOCK_METHOD1(setID, void(quint32 id));

    MOCK_CONST_METHOD0(pluginTypeIDs, QList<quint32>());

    MOCK_CONST_METHOD1(pluginTypeID,QList<quint32>(QString pluginName));

    MOCK_CONST_METHOD1(pluginName, QString(quint32 pluginTypeID));

    MOCK_CONST_METHOD1(hasPlugin, bool(quint32));

    MOCK_CONST_METHOD3(deliverMessage, void(quint64 srcPluginID, quint64 destPluginID, const QByteArray& message));

};

}



#endif // ICMOCKNODE_H
