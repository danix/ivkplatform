#ifndef ICCLIENTHANDSHAKETEST_H
#define ICCLIENTHANDSHAKETEST_H
#include "QObject"
#include "icclienthandshake.h"
#include "icmocknode.h"

namespace icrouting {

class ICHandshakeTest : public QObject
{
    Q_OBJECT
public:
ICHandshakeTest();
private slots:

    void testClientHandshake();

};

}


#endif // ICCLIENTHANDSHAKETEST_H
