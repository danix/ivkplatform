#ifndef TCPSTREAMTEST_H
#define TCPSTREAMTEST_H

#include <QObject>
#include <ictcpstream.h>
#include <QTcpServer>

/**
 * @brief Тест класса, реализующего обмен данными через TCP-сокет
 */
class TcpStreamTest : public QObject
{
    Q_OBJECT

    enum Tests
    {
        InitTest,
        OpenTest,
        WriteTest,
        ReadTest,
        CloseTest,
        HandleConnectionLostTest,
        TryConnectUnavailableTest
    };

public:
    TcpStreamTest(QObject* parent=0);
public slots:
    void onServerAcceptConnection();
    void onServerReadyRead();
    void onClientOpened();
    void onClientClosed();
    void onClientReadyRead();
    void onClientConnectTimeout();
private slots:
    /**
     * @brief 0. Инициализация TCP-соединения
     */
    void initializeConnection();
    /**
     * @brief 1. Открытие TCP-соединения
     */
    void openClientConnection();
    /**
     * @brief 2. Запись данных
     */
    void writeString();
    /**
     * @brief 3. Чтение данных
     */
    void readString();
    /**
     * @brief 4. Разрыв соединения
     */
    void closeClientConnection();
    /**
     * @brief 5. Обработка разрыва подключения
     */
    void handleConnectionLost();
    /**
     * @brief 6. Попытка подключения к несуществующему хосту
     */
    void tryConnectUnavailableHost();
private:
    inline bool success(){return m_success;}
    bool m_success;
    int m_currentTest;
    ICTcpStream m_stream;
    const quint16 testPort = 23231;
    QTcpServer m_server;
    QTcpSocket* m_serverSocket;
};

#endif // TCPSTREAMTEST_H
