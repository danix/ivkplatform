#ifndef COMPRESSENCODERTEST_H
#define COMPRESSENCODERTEST_H

#include <QObject>

/**
 * @brief The CompressEncoderTest class
 */
class EncoderTest : public QObject
{
    Q_OBJECT
public:
    explicit EncoderTest(QObject *parent = 0);
private slots:
    /**
     * @brief Формирует таблицу тестируемых кодировщиков
     */
    void testEncoder_data();
    /**
     * @brief Тест №1.Тестирует кодировщики потока данных
     */
    void testEncoder();
    /**
     * @brief Формирует таблицу тестируемых кодировщиков для теста с установкой ключа
     */
    void testEncoderWithKey_data();
    /**
     * @brief 2.Тест №2.Тестирует кодирование/декодирование данных с установкой ключа
     */
    void testEncoderWithKey();
};

#endif // COMPRESSENCODERTEST_H
