#include "diffiehellmantest.h"
#include <QtTest/QTest>

DiffieHellmanTest::DiffieHellmanTest(QObject *parent) :
    QObject(parent)
{
    connect(&m_clientKeyGenerator,SIGNAL(sendKeyRequest(ICPACKET)),&m_serverKeyGenerator,SLOT(handleKeyRequest(ICPACKET)));
    connect(&m_serverKeyGenerator,SIGNAL(sendKeyResponse(ICPACKET)),&m_clientKeyGenerator,SLOT(handleKeyResponse(ICPACKET)));
    connect(&m_clientKeyGenerator,SIGNAL(keyGenerated(QByteArray)),SLOT(onKeyGenerated(QByteArray)));
    connect(&m_serverKeyGenerator,SIGNAL(keyGenerated(QByteArray)),SLOT(onKeyGenerated(QByteArray)));
}

void DiffieHellmanTest::onKeyGenerated(QByteArray sessionKey)
{
    // добавить ключ в список результатов
    m_keys.append(sessionKey);
}

///////////////////// Tests ////////////////////////////

void DiffieHellmanTest::testKeyGeneration()
{
    // Начать процедуру генерации ключа
    m_clientKeyGenerator.generateKey();
    // Ждать пока будет сгенерирована пара
    QTRY_VERIFY_WITH_TIMEOUT(m_keys.length()==2,1000);
    // Сравнить результаты
    QCOMPARE(m_keys[0],m_keys[1]);
}
