#ifndef ICCHIPHERENCODER_H
#define ICCHIPHERENCODER_H

#include "icdataencoder.h"
#include <sosemanuk.h>

/**
 * @brief Класс, реализующий шифрование/дешифрование потока данных.
 *
 * Стандартная реализация использует алгоритм поточного шифрования "Сосеманук".
 * @ingroup core
 */
class ICChipherEncoder : public ICDataEncoder
{
    Q_OBJECT
public:
    explicit ICChipherEncoder(QObject* parent=0);
    ICChipherEncoder(const ICChipherEncoder& copy): ICDataEncoder(copy){Q_UNUSED(copy);}
    ~ICChipherEncoder(){}
    void setKey(const QByteArray &key) override; 
    QByteArray encode(const QByteArray &data) override; 
    QByteArray decode(const QByteArray &data,bool *ok=nullptr) override; 
private:
    sosemanuk_key_context m_keyContext;
};

#endif // ICCHIPHERENCODER_H
