#ifndef ICSECURESTREAMENCODER_H
#define ICSECURESTREAMENCODER_H
#include"icnetwork_global.h"
#include <iccompressencoder.h>
#include <icchipherencoder.h>

/**
 * @brief Класс, реализующий шифрование потока данных с предварительным сжатием.
 * 
 * Для сжатия применяются алгоритмы, реализованные в библиотеки QtCore.
 * Для шифрования применяется алгоритм поточного шифрования Сосеманук.
 * @ingroup core
 */
class ICNETWORKSHARED_EXPORT ICSecureStreamEncoder : public ICDataEncoder
{
    Q_OBJECT
public:
    explicit ICSecureStreamEncoder(QObject* parent=0);
    ICSecureStreamEncoder(const ICSecureStreamEncoder& copy):ICDataEncoder(copy){Q_UNUSED(copy);}
    virtual ~ICSecureStreamEncoder(){}

    void setKey(const QByteArray &key);
    QByteArray encode(const QByteArray &data);
    QByteArray decode(const QByteArray &data,bool* ok=0);
    ICRETCODE init(const QString &pref) override;
protected:
    inline ICCompressEncoder* compressEncoder() {return &m_compressEncoder;}
    inline ICChipherEncoder* chipherEncoder() {return &m_chipherEncoder;}
private:
    ICCompressEncoder m_compressEncoder;
    ICChipherEncoder m_chipherEncoder;
};

#endif // ICSECURESTREAMENCODER_H
