#ifndef UTILITIES_H
#define UTILITIES_H

#include "QString"
#include "icnetworkdetails.h"
#include "boost/ptr_container/ptr_unordered_map.hpp"

namespace icrouting {

ICNetworkDetails toICNetworkAddress(const QString &details);

QString toString(const ICNetworkDetails &details);

}

#endif // UTILITIES_H
