#ifndef ICBASENODESESSIONPRIVATE_H
#define ICBASENODESESSIONPRIVATE_H
#include "icobject.h"
#include "icremotenode.h"
#include "icsessionstate.h"

namespace icrouting {

class ICBaseLink;
class ICBaseLinkPrivate: public ICObject
{
    Q_OBJECT
public:
    ICBaseLinkPrivate(ICBaseLink*s);
    virtual ~ICBaseLinkPrivate();

    void open();

    void setConnection(ICConnection* c);

    ICConnection* connection()const{Q_CHECK_PTR(m_connection); return m_connection.data();}

    ICRemoteNode* node()const{Q_CHECK_PTR(m_rnode); return m_rnode.data();}

    ICNetworkDetails serverDetails()const{return m_rnodeServerDetails;}

    void setServerDetails(const ICNetworkDetails& addr){m_rnodeServerDetails = addr;}

    int priority()const{Q_ASSERT(m_priority != -1); return m_priority;}

    void setPriority(int p){m_priority = p;}

    bool isNodeUp()const{return m_rnode;}


    // implementation

    void changeState(ICBaseLinkState*nextState);

    void addNode(const ICNodeInfo &info);

    void removeNode(){m_rnode.reset(); emit nodeDown();}

    void addPlugins(const QStringList &list);

    void removePlugins(const QStringList &list);

    void close();

    void forwardPluginMessage(quint64 src, quint64 dest, const QByteArray& data){emit pluginMessage(src, dest, data);}

    bool isValid();

public slots:

    void onDataAvailable(const QByteArray& data);


signals:

    void nodeUp();

    void nodeDown();

    void closed();

    void aborted();

    void pluginsChanged();

    void pluginMessage(quint64 src, quint64 dest, const QByteArray& data);

private slots:
    void onLinkEstablished();

    void onLinkLost();

    virtual void doOnLinkLost() = 0;

private:
    virtual ICBaseLinkState* firstState() = 0;

private:


    QScopedPointer<ICRemoteNode, QScopedPointerDeleteLater> m_rnode;

    QScopedPointer<ICConnection, QScopedPointerDeleteLater> m_connection;

    ICBaseLinkState* m_state = nullptr;

    bool m_nodeUp = false;

    int m_priority = -1;

    ICNetworkDetails m_rnodeServerDetails;

    ICBaseLink* m_pub = nullptr;
};

}


#endif // ICBASENODESESSIONPRIVATE_H
