/**
 * @file icplatformlayer.h
 * @brief Описание API платформы
 * @copyright ivk-center.ru
 */

#ifndef ICPLATFORMLAYER_H
#define ICPLATFORMLAYER_H

#include <QObject>
#include <QByteArray>
#include "icplugincontext.h"
#include <icobject.h>
#include <icplatform_global.h>
//#include <icplugininterface_global.h>
//class ICPluginContext;
class ICPlatformLoader;
class ICPluginInterface;
namespace icrouting {
class ICRouter;
}

/**
 * @brief Интерфейс работы с ICPlatform.
 * 
 * API, которыми пользуются плагины.
 * @ingroup core 
 */
class ICPLATFORMSHARED_EXPORT ICPlatformLayer : public ICObject
{
    Q_OBJECT
public:
    ICPlatformLayer(QObject* parent=nullptr);
    ~ICPlatformLayer();

    // API
    // Routing information collection methods
    QList<quint64> pluginIDs(const QString& pluginName)const; ///< Получает идентификаторы плагинов с определенным именем со всех нодов.
    QList<quint64> pluginIDs()const;                   ///< Получает идентификаторы всех плагинов со всех нодов.
    quint64 thisPluginID()const;                       ///< Получает собственный идентификатор.
    QString nodeName(quint64 pluginID)const;           ///< Получает имя нода по идентификатору плагина.
    QString pluginName(quint64 pluginID)const;         ///< Получает имя плагина по идентификатору плагина.

    // Routing methods
    void sendMessage(quint64 destPluginID, const QByteArray& data = QByteArray())const;                  ///< Посылает сообщение плагину.
    quint64 generateUID();   ///< генерирует уникальный идентификатор.

    const ICPluginContext* context() const {return m_context.data();}
    friend class ICPlatformLoader;
    friend class icrouting::ICRouter;
private:
    QScopedPointer<ICPluginContext> m_context;
};

#endif // ICPLATFORMLAYER_H


