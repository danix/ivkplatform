/**
 * @file iclocalsettings.h
 * @brief Класс для хранения настроек в файле.
 * @copyright ivk-center.ru
 */

#ifndef ICLOCALSETTINGS_H
#define ICLOCALSETTINGS_H

#include "icsettings.h"
#include <QSettings>
#include <QMutex>

//#define IC_ORGANIZATION_NAME "ivk-center"

/**
 * @brief Класс локально хранимых настроек.
 *
 * Может работать с древовидной структурой настроек, т.е. один файл настроек может включать другие через группу [Include].
 * Например, файл с настройками, который включает в себя 2 других файла:
 * [Include]
 * mainSettings=/path/to/main/settings.ini
 * pluginSettings=/path/to/settings/of/plugins.ini
 * Часть файлов из всей структуры могут быть доступны только для чтения.
 * При попытке записи ключа в корневую группу будет проверено наличие такого ключа во всех файлах настроек, и если этот ключ уже существует,
 * то он будет переписан (если этот файл доступен для записи). Если же этого ключа нет, то он будет сохранен в корневом файле настроек.
 * Если корневой файл настроек недоступен для записи, то ключ будет сохранен в первом попавшемся файле из набора, запись в который разрешена.
 * Если производится попытка записи ключа в какую-либо группу, то опять же проверяется наличие этого ключа в каком-либо файле. Если ключ уже есть,
 * он переписывается (если есть возможность). Если ключ не найден, то он будет записан в первый попавшийся файл, запись в который разрешена.
 * При чтении ключа производится его поиск по всем файлам. Если найдено несколько таких ключей в разных файлах, то отбрасываются ключи из файлов,
 * доступных для записи (при наличии такого ключа в файлах, доступных только для чтения). Из оставшихся ключей возвращается первый попавшийся.
 * @ingroup settings core
 */
class ICSETTINGSSHARED_EXPORT ICLocalSettings : public ICSettings
{
    Q_OBJECT
public:
    ICLocalSettings(const QString& organization, const QString& application);
    ICLocalSettings(const ICLocalSettings& copy): ICSettings(copy.m_files[0]->organizationName(),copy.m_files[0]->applicationName()){}
    ICLocalSettings();
    virtual ~ICLocalSettings();
    QVariant value(const QString& key, const QVariant& defaultValue = QVariant()) const override;
    void setValue(const QString& key, const QVariant& value) override;
    void beginGroup(const QString prefix) override;
    void endGroup() override;
    QStringList childGroups() const override;
    QStringList childKeys() const override;
    QStringList allKeys() const override;
    void remove(const QString& key) override;
    void clear() override;
    QString fileName() const;
    void sync();
//    void beginReadArray(const QString& prefix);
//    void beginWriteArray(const QString& prefix);
//    void setArrayIndex(int index);
//    void endArray();
private:
    QString makeFullGroup() const;
    void parseSettingsFile(const QString& fname);  ///< парсит файл настроек и добавляет в m_files все включенные файлы. Кольца не детектируются.
    QStringList m_currentGroup; ///< Текущая группа настроек. Получается путем конкатенации всех элементов списка, начиная с первого.
    QList<QSettings*> m_files;  ///< Список всех файлов с настройками.
};

#endif // ICLOCALSETTINGS
