#ifndef ICTCPCLIENTSLOADER_H
#define ICTCPCLIENTSLOADER_H

#include "icconnectionfactory.h"

/**
  * @brief Класс-загрузчик клиентских TCP-подключений.
  * Читает конфиг и создает TCP-подключения с прописанными узлами.
  * Пример конфига:
  *
  * [connections]
  * size = 2                                        # 2 подключения
  *
  * [connections/1]
  * hostname = "ivk-center.com"
  * port = 23231                                    # TCP-порт по-умолчанию
  * encoder = "ICSecureStreamEncoder"               # Сжатие + шифр Сосеманук
  * keygenerator = "ICDiffieHellmanKeyGenerator"    # Протокол Диффи-Хельмана
  *
  * [connections/2]
  * hostname = "127.0.0.1"
  * secure = false                                  # Незащищенный протокол обмена
  * @ingroup core
  */
class ICTcpClientsLoader : public ICConnectionFactory
{
    Q_OBJECT
public:
    ICTcpClientsLoader(QObject *parent=0);
    /**
      * По-умолчанию, для всех созданных подключений фабрика является родителем.
      * Это означает, что все они будут удалены в деструкторе базового класса.
      * Если "клиент" намерен взять обязанности по удалению объектов подключений на себя,
      * то в обработчике сигнала newConnection() он должен вызвать:
      * connection->setParent(newParent), указав в качестве newParent, например, себя или NULL.
      */
    ~ICTcpClientsLoader(){}
    /**
     * @brief Читает конфиг и создает TCP-подключения с прописанными узлами и настройками.
     * 
     * По мере создания класс посылает сигнал newConnection().
     */
    void start();
};

#endif // ICTCPCLIENTSLOADER_H
