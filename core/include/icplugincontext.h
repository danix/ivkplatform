/**
 * @file icplatformlayer.h
 * @brief Описание API платформы
 * @copyright ivk-center.ru
 */
#ifndef ICPLUGINCONTEXT_H
#define ICPLUGINCONTEXT_H

#include <QObject>

namespace icrouting {
class ICRouter;
}

/**
 * @brief Класс контекста подключаемого модуля.
 * @ingroup core
 */
class ICPluginContext : public QObject
{
    Q_OBJECT
public:
    quint64 pluginAddress() const {return m_pluginAddress;} ///< адрес плагина 
    void setPluginAddress(quint64 addr) {m_pluginAddress = addr;} ///< устанавливает адрес плагина
    QString pluginName() const {return m_pluginName;} ///< имя плагина
    void setPluginName(const QString& name){m_pluginName = name;} ///< устанавливает имя плагина
    void setRouter(const icrouting::ICRouter*r){m_router = r;} ///< устанавливает рутер сообщений модулей
    const icrouting::ICRouter* router()const{return m_router;} ///< рутер сообщений

protected:
    quint64 m_pluginAddress;
    QString m_pluginName;
    const icrouting::ICRouter* m_router;
};

#endif // ICPLUGINCONTEXT_H
