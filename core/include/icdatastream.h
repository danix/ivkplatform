#ifndef ICDATASTREAM_H
#define ICDATASTREAM_H

#include <icobject.h>
#include <QVariant>

/**
  * @brief Интерфейс, описывающий методы связи текущего узла системы с другим узлом.
  *
  * Описывает методы передачи данных между узлами системы на нижнем уровне.
  * @ingroup core
  */
class ICDataStream : public ICObject
{
    Q_OBJECT
public:
    explicit ICDataStream( QObject* parent=0) : ICObject(parent){}
    virtual ~ICDataStream(){}
    /**
     * @brief Инициализация.
     */
    virtual void initialize() = 0;
    /**
     * @brief Инициализация.
     * @param  connectionAttribute Аттрибут соединения. Например, дескриптор сокета или
     * строка подключения.
     */
    virtual void setConnectionAttribute(QVariant connectionAttribute) = 0;
    /**
     * @brief Открыть поток
     * @return true, если соединение установлено, иначе - false
     */
    virtual bool open (bool outgoing) =0;
    /**
     * @brief Закрыть поток
     */
    virtual void close ( ) =0;
    /**
     * @brief Запись данных в поток
     * @return количество байт, записанных в поток
     * @param  data Данные
     */
    virtual quint64 write (const QByteArray& data ) =0;
    /**
     * @brief Чтение данных из буфера потока
     * @param  bytesToRead Размер данных (в байтах), которые необходимо вычитать из
     * буфера потока. Если 0, вычитывается все содержимое буфера.
     * @return массив прочитанных данных
     */
    virtual QByteArray read (quint64 bytesToRead = 0 ) = 0;

    virtual QString peerPortAddress()const = 0;

    virtual quint32 localAddress()const = 0;

signals:
    /**
     * @brief Сигнал о пополнении буфера входных данных
     */
    void readyRead ( );
    /**
     * @brief Сигнал о готовности потока к обмену
     */
    void opened ( );
    /**
     * @brief Сигнал о закрытие потока
     */
    void closed ( );
    /**
     * @brief Сигнал о таймауте установки подключения
     */
    void openTimeout();
};

#endif // ICDATASTREAM_H
