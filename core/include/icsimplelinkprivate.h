#ifndef ICSIMPLENODESESSIONPRIVATE_H
#define ICSIMPLENODESESSIONPRIVATE_H
#include "icbaselinkprivate.h"

namespace icrouting{

class ICSimpleLinkPrivate: public ICBaseLinkPrivate
{
    Q_OBJECT
public:
    ICSimpleLinkPrivate(ICBaseLink*s);
    ICBaseLinkState* firstState(){return new ICSimpleClientLinkVerificationState(this);}
    static int typePriority(){return 1;}
private slots:
    void doOnLinkLost(){emit aborted();}
};

}


#endif // ICSIMPLENODESESSIONPRIVATE_H
