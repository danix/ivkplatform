#ifndef ICPRIMARYNODESESSIONPRIVATE_H
#define ICPRIMARYNODESESSIONPRIVATE_H
#include "icdiscoverylinkprivate.h"
namespace icrouting {

class ICPrimaryLinkPrivate: public ICDiscoveryLinkPrivate
{
    Q_OBJECT
public:
    ICPrimaryLinkPrivate(ICBaseLink*s);
    ICBaseLinkState* firstState(){return new ICPrimaryClientLinkVerificationState(this);}
    static int typePriority(){return 2;}
private slots:
    void doOnLinkLost(){}
};

}





#endif // ICPRIMARYNODESESSIONPRIVATE_H
