#ifndef ICROUTER_H
#define ICROUTER_H
#include "QObject"
#include "icplugintype.h"
#include "icobject.h"
#include "icgridmanager.h"
#include "icnodeswitch.h"
#include "iclocalnode.h"

class ICPluginInterface;

namespace icrouting {

class ICRouter: public ICObject
{
    Q_OBJECT
public:

    ICRouter(QObject* parent=nullptr);
    ~ICRouter();

    ICRETCODE init(const QString& pref) override;

    void connectToGrid();

    void addLocalPLugins(const QList<ICPluginInterface*>& pluginList);

    void removelocalPlugins(const QList<ICPluginInterface*>& pluginList);



    //функции обслуживания локальных запросов плагинов

    QList<quint64> pluginIDs(QString pluginName, quint64 senderID)const; ///< Получает идентификаторы плагинов с определенным именем со всех нодов.
    QList<quint64> pluginIDs(quint64 senderID)const;                   ///< Получает идентификаторы всех плагинов со всех нодов.
    QString nodeName(quint64 pluginID)const;           ///< Получает имя нода по идентификатору плагина.
    QString pluginName(quint64 pluginID)const;         ///< Получает имя плагина по идентификатору плагина.

    bool route(quint64 srcPluginID, quint64 destPluginID, const QByteArray& message)const;

private:
    Q_DISABLE_COPY(ICRouter)

    void configureLocalNode(ICLocalNode*ln);

    QString createRandomName();

private slots:

    void onRouteRequested(quint64 srcPluginID, quint64 destPluginID, const QByteArray& message)const;

    void onNewRemoteNode(const ICRemoteNode *node);

    void onRemoteNodeLost(const ICRemoteNode *node);

signals:
    void routeRequested(quint64 srcPluginID, quint64 destPluginID, const QByteArray& message)const;
    void pluginsChanged();

private:
    QScopedPointer<ICNodeSwitch> m_switch;
    QScopedPointer<ICLocalNode> m_localNode;
};


}
#endif // ICDISPATCHER_H
