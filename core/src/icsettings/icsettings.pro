#-------------------------------------------------
#
# Project created by QtCreator 2014-01-29T12:03:03
#
#-------------------------------------------------
include(../../../platform_common.pri)
QT -= gui
TARGET = icsettings
TEMPLATE = lib
CONFIG += plugin

DEFINES += ICSETTINGS_LIBRARY

SOURCES += iclocalsettings.cpp \
#    icglobalsettings.cpp

HEADERS += $$CORE_INCLUDE/icsettings.h \
            $$CORE_INCLUDE/iclocalsettings.h 
#    ../../include/icglobalsettings.h
CONFIG += debug
