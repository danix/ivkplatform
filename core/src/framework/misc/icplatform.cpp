#include <icplatform.h>
#include <iclocalsettings.h>
#include <QCoreApplication>
#include <QDebug>
#include <QDateTime>

ICPlatform::ICPlatform()
{
    qsrand((uint)QDateTime::currentMSecsSinceEpoch());
    m_mainThread = QThread::currentThread();
    ICLocalSettings sett;
    connect(this, &ICPlatform::init, this, &ICPlatform::initPlatform, Qt::QueuedConnection);
    if(sett.value("separateThread", QVariant(false)).toBool())
    {
        qDebug()<<"moving platform to separate thread";
        m_platformThread = new QThread();
        moveToThread(m_platformThread);
        connect(m_platformThread, &QThread::started, this, &ICPlatform::init, Qt::QueuedConnection);
        m_platformThread->start();
    }
    else
    {
        m_platformThread = m_mainThread;
        emit init();
    }
}

ICPlatform::~ICPlatform()
{
    // здесь надо все четко освободить, но пока насрать
}

void ICPlatform::initPlatform()
{
    m_platformLoader.reset(new ICPlatformLoader(m_mainThread));
    QStringList args = QCoreApplication::arguments();
    if(args.count()>1)
        m_prefix = args.at(1);
    connect(m_platformLoader.data(), &ICPlatformLoader::platformLoaded, this, &ICPlatform::onPlatformLoaded, Qt::QueuedConnection);
    m_platformLoader->loadPlatform(m_prefix);
}

void ICPlatform::onPlatformLoaded(ICPluginLoader* loader)
{
    m_router.reset(new icrouting::ICRouter(this)); // раньше создавать нельзя, т.к. неизвестен m_prefix, который меняется в loadPlatform
    m_router->init(m_prefix);
    auto list = loader->loadedPlugins();
    m_router->addLocalPLugins(list);
    m_loader = loader;
    for(auto pl : list) // на самом деле пока хер знает, где это надо вызывать...
        pl->pushStart();
    m_router->connectToGrid();
    // здесь надо начинать устанавливать соединения с другими инстансами
}

