#include "icremotenode.h"
#include "iclocker.h"
#include "icmessages.h"
#include "icconnection.h"
#include "QDebug"
#include <ichash.h>

namespace icrouting {


void ICRemoteNode::setPlugins(const QStringList &list)
{
    QWriteLocker wlocker(ICLocker::routeTableLock());
    for(QString plugin : list)
        m_pluginNameByID.insert(icHash(plugin),plugin);
}

void ICRemoteNode::removePlugins(const QStringList &list)
{
    QWriteLocker wlocker(ICLocker::routeTableLock());
    for(QString plugin : list)
    {
        Q_ASSERT(m_pluginNameByID.contains(icHash(plugin)));
        m_pluginNameByID.remove(icHash(plugin));
    }
}

QList<quint32> ICRemoteNode::pluginTypeIDs()const
{
    return m_pluginNameByID.keys();
}

QList<quint32> ICRemoteNode::pluginTypeID(QString name)const
{
    QList<quint32> pId;
    return pId<<icHash(name);
}


QString ICRemoteNode::pluginName(quint32 pluginTypeID)const
{
   return m_pluginNameByID.value(pluginTypeID);
}

bool ICRemoteNode::hasPlugin(quint32 pluginTypeID) const
{
    return m_pluginNameByID.contains(pluginTypeID);
}


void ICRemoteNode::deliverMessage(quint64 srcPluginID, quint64 destPluginID, const QByteArray& data)const
{
    ICMessage message = ICMessage::normalMessage();
    ICPluginMessage plugmes;
    plugmes.source = srcPluginID;
    plugmes.target = destPluginID;
    plugmes.payload = data;
    message.setData(plugmes.toByteArray());
    m_connection->send(message.toByteArray());
}

bool operator ==(const ICRemoteNode& first,const ICRemoteNode& second)
{
    return (first.name()== second.name());
}

std::size_t hash_value(const ICRemoteNode &node)
{
    return qHash(node.name())^qHash(uint(node.id()));
}

}
