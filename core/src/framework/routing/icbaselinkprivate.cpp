#include "icbaselinkprivate.h"
#include "icconnection.h"
#include "icgridmanager.h"
#include "ichash.h"

namespace icrouting {

ICBaseLinkPrivate::ICBaseLinkPrivate(ICBaseLink* s)
{
    m_pub = s;
}

ICBaseLinkPrivate::~ICBaseLinkPrivate()
{
    if(m_state)
    {
        m_state->teardown();
        delete m_state;
    }
}

void ICBaseLinkPrivate::open()
{
    Q_CHECK_PTR(m_connection);
    m_connection->open();
}

void ICBaseLinkPrivate::setConnection(ICConnection* c)
{
    Q_CHECK_PTR(c);
    Q_ASSERT(!m_connection);
    m_connection.reset(c);
    connect(m_connection.data(), &ICConnection::received, this, &ICBaseLinkPrivate::onDataAvailable);
    connect(m_connection.data(), &ICConnection::connected, this, &ICBaseLinkPrivate::onLinkEstablished);
    connect(m_connection.data(), &ICConnection::disconnected, this, &ICBaseLinkPrivate::onLinkLost);
}

void ICBaseLinkPrivate::onLinkEstablished()
{
    m_state = firstState();
    m_state->initialize();
}

void ICBaseLinkPrivate::onDataAvailable(const QByteArray &data)
{
    Q_CHECK_PTR(m_state);
    ICMessage incomingMes(data);
    m_state->processMessage(incomingMes);
}

void ICBaseLinkPrivate::changeState(ICBaseLinkState *nextState)
{
    Q_CHECK_PTR(nextState);
    m_state->teardown();
    delete m_state;
    m_state = nextState;
    m_state->initialize();
}

void ICBaseLinkPrivate::addNode(const ICNodeInfo &info)
{
    m_rnode.reset(new ICRemoteNode);
    m_rnode->setName(info.m_nodeName);
    m_rnode->setID(icHash(info.m_nodeName));
    m_rnode->setPlugins(info.m_pluginNames);
    m_rnode->setConnection(m_connection.data());
    m_nodeUp = true;
    emit nodeUp();
}

void ICBaseLinkPrivate::onLinkLost()
{
    if(m_state)
    {
        m_state->teardown();
        delete m_state;
        m_state = nullptr;
    }
    if(m_rnode)
    {
        emit nodeDown();
        m_rnode.reset();
    }
    doOnLinkLost();
}

void ICBaseLinkPrivate::close()
{
    m_connection->setAutoReconnect(false);
    m_connection->close();
    emit closed();
}

void ICBaseLinkPrivate::addPlugins(const QStringList &list)
{
    m_rnode->setPlugins(list);
    emit pluginsChanged();
}

void ICBaseLinkPrivate::removePlugins(const QStringList &list)
{
    m_rnode->removePlugins(list);
    emit pluginsChanged();
}

bool ICBaseLinkPrivate::isValid()
{
    return ICGridManager::instance()->sessionValid(m_pub);
}


}
