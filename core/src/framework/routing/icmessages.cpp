#include "icmessages.h"
#include "QIODevice"

namespace icrouting {


ICMessage::ICMessage()
{

}

ICMessage::ICMessage(const QByteArray &data)
{
    QDataStream stream(data);
    stream>>*this;
}

void ICMessage::setData(const QByteArray &data)
{
    m_payload = data;
}

QByteArray ICMessage::toByteArray()const
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::ReadWrite);
    stream<<*this;
    return data;
}



QDataStream& operator<<(QDataStream& stream, const ICMessage& message)
{
    stream<<message.m_type;
    stream<<message.m_payload;
    return stream;
}

QDataStream& operator>>(QDataStream& stream, ICMessage& message)
{
    int type;
    stream>>type;
    message.m_type = ICMessage::Type(type);
    stream>>message.m_payload;
    return stream;
}


ICMessage ICMessage::primaryLinkVerificationMessage()
{
    ICMessage message;
    message.m_type = PrimaryLinkValidation;
    return message;
}

ICMessage ICMessage::simpleLinkVerificationMessage()
{
    ICMessage message;
    message.m_type = SimpleLinkValidation;
    return message;
}


ICMessage ICMessage::discoveryMessage()
{
    ICMessage message;
    message.m_type = Discovery;
    return message;
}

ICMessage ICMessage::infoExchangeMessage()
{
    ICMessage message;
    message.m_type = InfoExchange;
    return message;
}

ICMessage ICMessage::pluginsInMessage()
{
    ICMessage message;
    message.m_type = PluginsIn;
    return message;
}

ICMessage ICMessage::pluginsOutMessage()
{
    ICMessage message;
    message.m_type = PluginsOut;
    return message;
}

ICMessage ICMessage::normalMessage()
{
    ICMessage message;
    message.m_type = Normal;
    return message;
}


ICPluginMessage::ICPluginMessage(const QByteArray& data)
{
    QDataStream stream(data);
    stream>>source>>target>>payload;
}

QByteArray ICPluginMessage::toByteArray()const
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);
    stream<<source<<target<<payload;
    return data;
}


ICLinkVerificationInfo::ICLinkVerificationInfo()
{

}


ICLinkVerificationInfo::ICLinkVerificationInfo(const QByteArray &data)
{
    QDataStream stream(data);
    stream>>name>>port;
}

QByteArray ICLinkVerificationInfo::toByteArray()const
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::ReadWrite);
    stream<<name<<port;
    return data;
}

ICLinkStatus::ICLinkStatus(const QByteArray &data)
{
    QDataStream stream(data);
    stream>>valid;
}

QByteArray ICLinkStatus::toByteArray()const
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);
    stream<<valid;
    return data;
}


ICNodeInfo::ICNodeInfo()
{

}

ICNodeInfo::ICNodeInfo(const QByteArray &info)
{
    QDataStream stream(info);
    stream>>*this;
}

QByteArray ICNodeInfo::toByteArray()const
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::ReadWrite);
    stream<<*this;
    return data;
}

QDataStream& operator<<(QDataStream& stream, const ICNodeInfo& info)
{
    stream<<info.m_nodeName<<info.m_pluginNames;
    return stream;
}

QDataStream& operator>>(QDataStream& stream, ICNodeInfo& info)
{
    stream>>info.m_nodeName>>info.m_pluginNames;
    return stream;
}


ICDiscoveryInfo::ICDiscoveryInfo()
{

}

ICDiscoveryInfo::ICDiscoveryInfo(const QByteArray &info)
{
    QDataStream stream(info);
    int count;
    stream>>count;
    for(int i = 0; i < count; ++i)
    {
        ICNetworkDetails addr;
        stream>>addr;
        m_addresses.push_back(addr);
    }
}

QByteArray ICDiscoveryInfo::toByteArray()const
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::ReadWrite);
    stream<<(int)m_addresses.size();
    for(size_t i = 0; i < m_addresses.size(); ++i)
        stream<<m_addresses.at(i);
    return data;
}

}
