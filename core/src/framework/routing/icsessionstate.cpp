#include "icsessionstate.h"
#include "utilities.h"
#include "icconnection.h"
#include "icgridmanager.h"
#include "icprimarylinkprivate.h"

namespace icrouting {

class ICPrimaryLink;

void ICBaseLinkState::initialize()
{
    begin();
}

void ICBaseLinkState::teardown()
{
    end();
}

void ICLinkValidationState::begin()
{
    ICMessage message  = createMessage();
    message.setData(ICGridManager::instance()->linkVerificationInfo().toByteArray());
    m_session->connection()->send(message.toByteArray());
}

void ICLinkValidationState::end()
{
    qDebug()<<"end Link Validation state";
}

void ICLinkValidationState::processMessage(const ICMessage &message)
{
    ICLinkStatus link(message.data());
    if(link.valid)
    {
        ICBaseLinkState* ns = nextState();
        m_session->changeState(ns);
    }
    else
        m_session->close();
}

ICMessage ICPrimaryClientLinkVerificationState::createMessage()
{
    qDebug()<<"begin Client Primary Link Validation state";
    return ICMessage::primaryLinkVerificationMessage();
}

ICBaseLinkState* ICPrimaryClientLinkVerificationState::nextState()
{
    return new ICDiscoveryStateClient(m_session);
}


ICMessage ICSimpleClientLinkVerificationState::createMessage()
{
    qDebug()<<"begin Client Simple Link Validation state";
    return ICMessage::simpleLinkVerificationMessage();
}

ICBaseLinkState* ICSimpleClientLinkVerificationState::nextState()
{
    return new ICInfoExchangeStateClient(m_session);
}

void ICDiscoveryState::sendDiscoveryInfo()
{
    ICMessage message = ICMessage::discoveryMessage();
    ICDiscoveryInfo info = ICGridManager::instance()->discoveryInfo(m_session->serverDetails());
    message.setData(info.toByteArray());
    m_session->connection()->send(message.toByteArray());
}

ICDiscoveryLinkPrivate* ICDiscoveryState::mySession()
{
    return static_cast<ICDiscoveryLinkPrivate*>(m_session);
}



void ICDiscoveryStateClient::begin()
{
    sendDiscoveryInfo();
    qDebug()<<"begin Client Discovery state";
}

void ICDiscoveryStateClient::end()
{
    qDebug()<<"end Client Discovery state";
}

void ICDiscoveryStateClient::processMessage(const ICMessage &message)
{
    Q_ASSERT(message.m_type == ICMessage::Discovery);
    ICDiscoveryInfo discInfo(message.data());
    mySession()->provideDiscoveryInfo(discInfo.m_addresses);
    ICInfoExchangeStateClient *nextState = new ICInfoExchangeStateClient(m_session);
    mySession()->changeState(nextState);
}

void ICDiscoveryStateServer::begin()
{
    qDebug()<<"begin Server Discovery State";
}

void ICDiscoveryStateServer::end()
{
    qDebug()<<"end Server Discovery State";
}

void ICDiscoveryStateServer::processMessage(const ICMessage &message)
{
    Q_ASSERT(message.m_type == ICMessage::Discovery);

    ICDiscoveryInfo discInfo(message.data());
    mySession()->provideDiscoveryInfo(discInfo.m_addresses);
    sendDiscoveryInfo();
    ICInfoExchangeStateServer *nextState = new ICInfoExchangeStateServer(m_session);
    mySession()->changeState(nextState);
}

// ICInfoExchangeState
void ICBaseInfoExchangeState::parseNodeInfo(const ICMessage &message)
{
    ICNodeInfo info(message.data());
    m_session->addNode(info);
}

void ICBaseInfoExchangeState::sendNodeInfo()
{
    ICMessage message = ICMessage::infoExchangeMessage();
    message.setData(ICGridManager::instance()->localNodeInfo().toByteArray());
    m_session->connection()->send(message.toByteArray());
}

//ICInfoExchangeStateServer
void ICInfoExchangeStateServer::begin()
{
    qDebug()<<"begin Server Info Exchange state";
}

void ICInfoExchangeStateServer::end()
{
    qDebug()<<"end Server Info Exchange state";
}

void ICInfoExchangeStateServer::processMessage(const ICMessage &message)
{     Q_ASSERT(message.m_type == ICMessage::InfoExchange);
      sendNodeInfo();
        parseNodeInfo(message);
          ICNormalState *nextState = new ICNormalState(m_session);
            m_session->changeState(nextState);
}


//ICInfoExchangeStateClient
void ICInfoExchangeStateClient::begin()
{
    sendNodeInfo();
    qDebug()<<"begin Client Info Exchange state";
}

void ICInfoExchangeStateClient::end()
{
    qDebug()<<"end Client Info Exchange state";
}

void ICInfoExchangeStateClient::processMessage(const ICMessage &message)
{
    parseNodeInfo(message);
    ICNormalState *nextState = new ICNormalState(m_session);
    m_session->changeState(nextState);
}


void ICNormalState::begin()
{
    qDebug()<<"begin Normal state";
}

void ICNormalState::end()
{
    qDebug()<<"end Normal state";
}

void ICNormalState::processMessage(const ICMessage &message)
{
    Q_ASSERT(message.m_type == ICMessage::Normal ||message.m_type == ICMessage::PluginsIn || message.m_type == ICMessage::PluginsOut);
    if(message.m_type == ICMessage::Normal)
    {
        ICPluginMessage pm(message.data());
        m_session->forwardPluginMessage(pm.source, pm.target, pm.payload);
    }
    else
        parseServiceMessage(message);
}

void ICNormalState::parseServiceMessage(const ICMessage &message)
{
    QStringList plugins = parsePluginList(message);
    switch (message.m_type) {
    case ICMessage::PluginsIn:
        m_session->addPlugins(plugins);
        break;
    case ICMessage::PluginsOut:
        m_session->removePlugins(plugins);
    default:
        break;
    }
}

QStringList ICNormalState::parsePluginList(const ICMessage &message)
{
    QDataStream stream(message.m_payload);
    QStringList plList;
    stream>>plList;
    return plList;
}

}
