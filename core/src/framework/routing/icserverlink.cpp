#include "icserverlink.h"

namespace icrouting {

ICServerLink::ICServerLink():ICDiscoveryLink(new ICServerLinkPrivate(this))
{
    connect(myImple(), &ICServerLinkPrivate::addressResolved, this , &ICServerLink::peerServerAddressResolved);
}

}


