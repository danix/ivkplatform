include(../../../platform_common.pri)

QT += core network
QT -= gui
TEMPLATE = lib
TARGET = ivkplatform
CONFIG += plugin

include(plugininterfaces/plugininterfaces.pri)
#include(network/network.pri)
include(routing/routing.pri)
include(misc/misc.pri)

LIBS += -L$$DESTDIR -licobject -licsettings -licnetwork

