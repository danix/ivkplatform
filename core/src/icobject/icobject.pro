include(../../../platform_common.pri)

TARGET = icobject
TEMPLATE = lib
CONFIG += plugin
QT -= gui
DEFINES += ICOBJECT_LIBRARY

HEADERS += $$CORE_INCLUDE/icobject.h \
           $$CORE_INCLUDE/icerror.h \
           $$CORE_INCLUDE/icerrorhandler.h \
           $$CORE_INCLUDE/icerrornotifier.h \
           $$CORE_INCLUDE/icobjectmacro.h \
           $$CORE_INCLUDE/icobjectsettings.h \
    ../../include/icerrorreporter.h

SOURCES += icobject.cpp\
    icerrorhandler.cpp \
    icobjectsettings.cpp \
    icerrornotifier.cpp

LIBS += -L$$DESTDIR -licsettings
CONFIG += debug
