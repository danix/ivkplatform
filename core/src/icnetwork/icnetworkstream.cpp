#include <icnetworkstream.h>
#include <QStringList>
#include <QHostAddress>

void ICNetworkStream::setConnectionAttribute(QVariant connectionAttribute)
{
    // Парсинг атрибутов
    if(connectionAttribute.type()==QVariant::String)
    {
        // Строка подключения <Имя/IP>:<Порт>
        QStringList vals = connectionAttribute.toString().split(":");
        Q_ASSERT(vals.length()==2);
        if(vals.length()!=2)
            return;
        m_hostname = vals[0];
        bool ok = false;
        m_port = vals[1].toInt(&ok);
        Q_ASSERT(ok);
    }
    else if(connectionAttribute.type()==QVariant::ULongLong)
    {
        // Дескриптор сокета
        bool ok = false;
        ulong ulongAttrib = connectionAttribute.toULongLong(&ok);
        Q_ASSERT(ok);
        if(!ok)
            return;
        auto sock = socket();
        Q_ASSERT(sock);
        if(!sock)
            return;
        quintptr socketDescriptor = (quintptr)ulongAttrib;
        sock->setSocketDescriptor(socketDescriptor);
        m_hostname = sock->peerAddress().toString();
        m_port = sock->peerPort();
    }
}

bool ICNetworkStream::open(bool outgoing)
{
    if(!ICIOStream::open(outgoing))
        return false;
    if(outgoing)
    {
        //Если соединение исходящее, попытаться подключиться
        QAbstractSocket* sock = socket();
        Q_ASSERT(sock);
        if(!sock)
            return false;
        sock->connectToHost(QHostAddress(m_hostname),m_port);
    }
    return true;
}

void ICNetworkStream::close()
{
    // закрыть сокет
    QAbstractSocket* sock = socket();
    Q_ASSERT(sock);
    if(!sock)
        return;
    sock->disconnectFromHost();
}

QString ICNetworkStream::peerPortAddress()const
{
    QString fullAddr("%1 : %2");
    return fullAddr.arg(m_hostname).arg(m_port);
}
