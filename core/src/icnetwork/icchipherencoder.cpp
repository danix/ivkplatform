#include "icchipherencoder.h"
#include <QDebug>
#include <QCryptographicHash>

/** Вектор инициализации.
 * 16 байт */
unsigned char g_sosemanukIV[] = {
    0x8,0xD,0x9,0x0,
    0x2,0xE,0x2,0x6,
    0x3,0xA,0x9,0x3,
    0x1,0xD,0x6,0xFF
};

ICChipherEncoder::ICChipherEncoder(QObject* parent)
    : ICDataEncoder(parent)
{
}

void ICChipherEncoder::setKey(const QByteArray &key)
{
    bool ok =false;
    auto hshAlgo = setting("security/keyHashAlgorithm",QCryptographicHash::Sha3_256).toInt(&ok);
    QByteArray resultKey;
    if(ok)
    {
        //вычислить хэш
        QCryptographicHash hash((QCryptographicHash::Algorithm)hshAlgo);
        hash.addData(key);
        resultKey = hash.result();
        if(resultKey.length()>32)
            resultKey = resultKey.left(32);
    }
    else if(key.length()>32)
    {
        // или просто обрезать
        resultKey = key.left(32);
    }
    //образ назначить ключом шифрования
    sosemanuk_schedule(&m_keyContext,(unsigned char*)resultKey.data(),resultKey.size());
}

QByteArray ICChipherEncoder::encode(const QByteArray &data)
{
    // инициализировать шифр
    sosemanuk_run_context runContext;
    sosemanuk_init(&runContext,&m_keyContext,g_sosemanukIV,sizeof(g_sosemanukIV));
    // копировать содержимое массива в буффер
    QByteArray result = data;
    auto buffer = (unsigned char*)result.data();
    // выполнить шифрование
    sosemanuk_encrypt(&runContext,buffer,buffer,result.size());
    return result;
}

QByteArray ICChipherEncoder::decode(const QByteArray &data,bool *ok)
{
    if(ok)
        *ok = true;
    return encode(data);
}


