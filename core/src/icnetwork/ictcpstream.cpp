#include "ictcpstream.h"

ICTcpStream::ICTcpStream(QObject* parent)
    :ICNetworkStream(parent)
{
    m_socket = new QTcpSocket();
}

void ICTcpStream::initialize()
{
    ICNetworkStream::initialize();
    // Подписать обработчик сигнала stateChanged
    Q_ASSERT(m_socket);
    if(!m_socket)
        return;
    connect(m_socket,&QTcpSocket::stateChanged, this, &ICTcpStream::onStateChanged);
}

ICTcpStream::~ICTcpStream()
{
    if(m_socket)
        m_socket->deleteLater();
}

void ICTcpStream::onStateChanged(QAbstractSocket::SocketState state)
{
    //если соединение установлено
    if(state == QAbstractSocket::ConnectedState)
        emit opened();
    // иначе, если разорвано
    else if(state == QAbstractSocket::UnconnectedState)
    {
        m_socket->close();
        emit closed();
    }
}
