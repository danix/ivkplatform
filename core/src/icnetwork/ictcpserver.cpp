#include "ictcpserver.h"
#include <ictcpstream.h>
#include <icdiffiehellmankeygenerator.h>
#include <icsecurestreamencoder.h>

void ICTcpListener::incomingConnection(qintptr socketDescriptor)
{
    // преобразовать дескриптор сокета
    qulonglong validDescriptor = (qulonglong)socketDescriptor;
    // сообщить о новом входящем подключении фабрике
    emit accepted(validDescriptor);
}

ICTcpServer::ICTcpServer(QObject* parent)
    :ICConnectionFactory(parent)
{
    //    m_secure = setting("server/secure","true").toBool();
    // подписаться к сигналу о входящем подключении к объекту-слушателю
    connect(&m_listener,&ICTcpListener::accepted, this, &ICTcpServer::createConnection);
}

ICRETCODE ICTcpServer::init(const QString &prefix)
{
    return ICObject::init(prefix);
}

void ICTcpServer::createConnection(qulonglong socketDescriptor)
{
    // инициализировать объект серверного TCP-подключения
    auto connection = new ICConnection(socketDescriptor,this);
    connection->init(settingsPrefix());
    connection->setDataStream(new ICTcpStream(connection));
    if(secure())
    {
        connection->setKeyGenerator(new ICDiffieHellmanKeyGenerator(connection));
        connection->setDataEncoder(new ICSecureStreamEncoder(connection));
    }
    // открыть подключение
    qDebug()<<"accept connection";
    connection->open();
    // сообщить о новом подключении
    emit newConnection(connection);
}

void ICTcpServer::start()
{
    // прочитать из настроек порт для входящих TCP-подключений

    uint port;
    bool ok = false;
    port = setting("connectivity/server/tcpPort",IC_DEFAULT_TCP_PORT).toUInt(&ok);

    if(!ok || port>0xFFFF)
    {
        issueWarning("TCP-сервер: некорректное значение TCP-порта");
        return;
    }

    m_serverPort = port;
    // слушать сокет
    if(m_listener.listen(QHostAddress::Any,(quint16)port))
        qDebug()<<"Node listening on port "<<port;
    else
        qDebug()<<m_listener.serverError();
}

void ICTcpServer::stop()
{
    // закрыть сокет
    m_listener.close();
}
